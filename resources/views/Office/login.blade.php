<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Sign In | Office</title>
        <!-- Favicon-->
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
           <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
    <!-- Google Fonts -->
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core Css -->
        <link href="/Backend/webadmin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Waves Effect Css -->
        <link href="/Backend/webadmin/plugins/node-waves/waves.css" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="/Backend/webadmin/plugins/animate-css/animate.css" rel="stylesheet" />
        <!-- Morris Chart Css-->
        <!-- Custom Css -->
        <link href="/Backend/webadmin/css/style.css" rel="stylesheet">
    </head>
    
    <body class="login-page">
        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);">Tapmi Office</a>
             <!--    <small>Visitor Managment System</small> -->
            </div>
            <div class="card">
                <div class="body">
                    <form id="sign_in" method="POST" action="/office/auth-login">
                        {{csrf_field()}}                    
                        <div class="msg">Sign in to start your session</div>
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" placeholder="Username" value="@if(Session::get('loggd-username')){{Session::get('loggd-username') }}@endif" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                                {{--<input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                                <label for="rememberme">Remember Me</label>--}}
                            </div>
                            <div class="col-xs-4">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                            </div>
                        </div>
                        @if(Session::has('login-error'))
                        <p class="alert alert-danger">{{ Session::get('login-error') }}</p>
                        @endif
                        {{--<div class="row m-t-15 m-b--20">
                            <div class="col-xs-6">
                                <a href="sign-up.html">Register Now!</a>
                            </div>
                            <div class="col-xs-6 align-right">
                                <a href="forgot-password.html">Forgot Password?</a>
                            </div>
                        </div>--}}
                    </form>
                </div>
            </div>
        </div>
        <script src="/Backend/webadmin/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core Js -->
        <script src="/Backend/webadmin/plugins/bootstrap/js/bootstrap.js"></script>
        <!-- Waves Effect Plugin Js -->
        <script src="/Backend/webadmin/plugins/node-waves/waves.js"></script>
        <!-- Custom Js -->
        <script src="/Backend/webadmin/js/admin.js"></script>
        <script src="/Backend/webadmin/js/signin.js"></script>
    </body>
</html>