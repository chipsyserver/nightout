<!DOCTYPE html>
<html ng-app="nightout">
    @include('Warden.header')
 

    <body class="theme-red">
        <style type="text/css">
            .content table {
            font-size: 12px !important; 
            }
        </style>
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container chng-width-navbar">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <!-- <a class="navbar-brand" href="/vms/visitors">
                        <img src="/Backend/webadmin/images/tapmi-logo.png" alt=""> 
                        Tapmi VMS
                    </a> -->
                    <a class="navbar-brand" href="/vms/visitors">
                        <img src="/Backend/webadmin/images/tapmi-brand.png" width="40px" />  
                        <b> TAPMI PGP </b>
                    </a>
                </div>                
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                    <li>
                                <a href="/warden/nightout">
                                  Wardens
                                </a>
                            </li>
                             <li>
                                <a href="/warden/dashbord">
                                  Dashbord
                                </a>
                            </li>
                            
                     <!--  <li class="dropdown menu-dropdown" >
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" id='selected-menu'>
                            @if (Request::is('vms/home'))
                                Home
                                <span class="caret"></span>
                            @elseif (Request::is('vms/visitors'))
                                <span>Visitors</span>
                                <span class="caret"></span>
                            @elseif (Request::is('vms/users'))
                                <span>Users</span>
                                <span class="caret"></span>
                            @else 
                                <span>Go to</span>
                                <span class="caret"></span>
                            @endif
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/vms/home">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="/vms/visitors">
                                    Visitors
                                </a>
                            </li>
                            <li>
                                <a href="/vms/users">
                                    Users
                                </a>
                            </li>
                        </ul>
                      </li> -->
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="nav-collapse4">
                        {{--    <li>
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">notifications</i>
                                <span class="label-count">7</span>
                            </a>
                            <ul class="dropdown-menu">
                            </ul>
                        </li>--}}
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span> 
                        <strong>{{Auth::user()->username}}</strong>
                        <span class="caret"></span></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="/warden/settings">Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="/warden/signout">Logout</a></li>
                          </ul>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>
        <div class="container chg-width">
            @yield('content')
        </div>

        {{--<footer>
          <div class="container">
            <p class="text-p text-right">&copy; {{date('Y')}} <a href="http://chipsy.in">chipsy.in Design</a>. All rights reserved.
            <!--<a href="https://www.facebook.com/Helsespesialisten-448789105236638/" target="_blank"><i class="fa fa-facebook"></i>Follow us on Facebook</a>-->
          </div>
        </footer>--}}
       
    </body>
</html>