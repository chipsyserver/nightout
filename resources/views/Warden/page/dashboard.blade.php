   @extends('Nightout.layouts.master_layout')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>
        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href=''">
                <div title='Go to visitors page' class="info-box bg-pink hover-expand-effect pointer-icon">
                    <div class="icon">
                        <i class="material-icons"> person</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Request</div>
                        <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">{{$total_users}}</div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='/warden/nightout'">
                <div title='Go to visitors page' class="info-box bg-orange hover-expand-effect pointer-icon">
                    <div class="icon">
                        <i class="material-icons">do_not_disturb</i>
                    </div>
                    <div class="content">
                        <div class="text">Pending</div>
                        <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">{{$pending}}</div>
                    </div>
                </div>
            </div>
             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='/warden/nightout#profile_with_icon_title'">
                <div title='Go to visitors page' class="info-box bg-light-green hover-expand-effect pointer-icon">
                    <div class="icon">
                        <i class="material-icons  ">beenhere</i>
                    </div>
                    <div class="content">
                        <div class="text">Approved</div>
                        <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">{{$aproved}}</div>
                    </div>
                </div>
            </div>
             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='/warden/nightout#messages_with_icon_title'">
                <div title='Go to users page' class="info-box bg-red hover-expand-effect pointer-icon">
                    <div class="icon">
                        <i class="material-icons"> backspace</i>
                    </div>
                    <div class="content">
                        <div class="text">Rejected</div>
                        <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">{{$rejected}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection