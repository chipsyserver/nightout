@extends('Nightout.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
<section class="content" ng-controller="VisitorsController">
   <div class="container-fluid">
      <div class="block-header">
      </div>

               <div class="row clearfix">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="width: 103%;   margin-left: -25px;
" >
                      <div class="card userid" id="{{Auth::guard('nightout')->user()->_id}}">
                        <div class="header">
                           <h2>
                              <span>Request (<%totalVisitors%>)</span>
                                <p class="pull-right">
                                <label for="search" class="small-label" style="    position: absolute;
    margin-left: -69px;
    margin-top: 8px;">Search: </label>
                                <select name="pgp" class="form-control show-tick">
                                    <option value="0">Requested</option>
                                    <option value="1">PGP1</option>
                                    <option value="2">PGP2</option>
                                </select>
                               
                               
                            </p>
                           </h2>

                       
                        </div>
                        <div class="body">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs status" name="status"  role="tablist">
                              <li id="0"  role="presentation" class="active">
                                 <a href="#home_with_icon_title" data-toggle="tab">
                                 <i class="material-icons" style="color: orange">do_not_disturb</i><b style="color: orange">Pending</b>
                                 </a>
                              </li>
                              <li id="1" role="presentation">
                                 <a href="#profile_with_icon_title" data-toggle="tab">
                                 <i class="material-icons" style="color: #64b964">check</i>      <b style="color: #64b964">No Class</b>
                                 </a>
                              </li>
                              <li id="-2" role="presentation">
                                 <a href="#messages_with_icon_title" data-toggle="tab" >
                                 <i class="material-icons" style="color: #e44747">close</i> 
                                  <b style="color: #e44747"> Class</b>
                                 </a>

                              </li>
                           </ul>
                           <!-- Tab panes -->
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                 <div class=" body">
                                    <table class="table table-bordered table-striped table-hover ">
                                       <thead>
                                          <tr>
                                             <th width="5%">Sl No</th>
                                            <!--  <th width="5%">User_id</th> -->
                                             <th width="5%">Roll_No</th>
                                             <th width="5%">Name_Of_The Student</th>
                                             <th>Sex</th>
                                             <th>Email_Id</th>
                                             <th>Mobile_No</th>
                                             <th>Terms Credit</th>
                                             <th>CFR</th>
                                             <!-- 
                                                <th>Request_Date &Time </th>
                                                <th>Placed & Unplaced</th> -->
                                             <th>Place of Visit </th>
                                             <th>Purpose of Visit </th>
                                             <th width="15%"> Request_Date  </th>
                                             <th width="15%"> Return_Date </th>
                                             <!--  <th>Alternate Contact Number</th> -->
                                             <th>Hostel Number</th>
                                             <th>Room Number</th>
                                             <!--   <th>Declaration acceptance by the Student</th>
                                                <th>No of Late Enteries</th> -->
                                             <!--     <th >Status</th> -->

                                             <th >Action</th>
                                             <!--  <th>More</th> -->
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr ng-repeat="visitor in visitors" id="<% visitor._id %>"   >
                                             <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                         <!--     <td ><% visitor._id %></td> -->
                                             <td><%visitor.requester.rollNo %></td>
                                             <td><%visitor.requester.name %></td>
                                             <td><%visitor.requester.gender %></td>
                                             <td><%visitor.requester.email %></td>
                                             <td><%visitor.requester.mobileNumber %></td>
                                            <td ng-if='visitor.requester.placed=="1"' > <b style="color:red">P</b></td>
                                      <td ng-if='visitor.requester.placed!="1"'><%visitor.requester.termCredits %></td>
                                             <td><%visitor.requester.carryForward %></td>
                                             <td><%visitor.placeOfVisit %></td>
                                             <td><%visitor.purposeVisit %></td>
                                             <td><% visitor.requested_on1 %></td>
                                             <td><% visitor.requested_on %></td>
                                             <td><%visitor.hosteNumber %></td>
                                             <td><%visitor.roomNumber %></td>
                                             <td >
                                                <div class="dropdown">
                                                   <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                   <span class="caret"></span></button>
                                                   <ul class="dropdown-menu action" id="<% visitor._id %>" >
                                                       <li    id="-2"   ><a href="javascript:void(0);"  >Class</a></li>
   
 
    
                                                      <li ng-if="visitor.status !='1'"  id="1"><a href="javascript:void(0);" >No Class</a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr ng-hide="visitors.length">
                                             <td colspan="15" align="center"><b>No visitors to display</b></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div >Showing <%visitorsFrom %> to <%visitorsTo%> of <% totalVisitors %> entries</div>
                                       </div>
                                       <div class="col-md-6 text-right">
                                          <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                                 <div class="table-responsive body">
                                    <table class="table table-bordered table-striped table-hover ">
                                       <thead>
                                          <tr>
                                             <th width="5%">Sl No</th>
                                           <!--   <th width="5%">User_id</th> -->
                                             <th width="5%">Roll_No</th>
                                             <th width="5%">Name_Of_The Student</th>
                                             <th>Sex</th>
                                             <th>Email_Id</th>
                                             <th>Mobile_No</th>
                                             <th>Terms Credit</th>
                                             <th>CFR</th>
                                             <!-- 
                                                <th>Request_Date &Time </th>
                                                <th>Placed & Unplaced</th> -->
                                             <th>Place of Visit </th>
                                             <th>Purpose of Visit </th>
                                             <th width="15%"> Request_Date  </th>
                                             <th width="15%"> Return_Date </th>
                                             <!--  <th>Alternate Contact Number</th> -->
                                             <th>Hostel Number</th>
                                             <th>Room Number</th>
                                             <!--   <th>Declaration acceptance by the Student</th>
                                                <th>No of Late Enteries</th> -->
                                             <!--     <th >Status</th> -->
                                          
                                             <th >Action</th>
                                             <!--  <th>More</th> -->
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr ng-repeat="visitor in visitors" id="<% visitor._id %>"    style="visibility: show;">
                                             <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                         <!--     <td ><% visitor._id %></td> -->
                                             <td><%visitor.requester.rollNo %></td>
                                             <td><%visitor.requester.name %></td>
                                             <td><%visitor.requester.gender %></td>
                                             <td><%visitor.requester.email %></td>
                                             <td><%visitor.requester.mobileNumber %></td>
                                       <td ng-if='visitor.requester.placed=="1"'> <b style="color:red">P</b></td>
                                      <td ng-if='visitor.requester.placed!="1"'><%visitor.requester.termCredits %></td>
                                             <td><%visitor.requester.carryForward %></td>
                                             <td><%visitor.placeOfVisit %></td>
                                             <td><%visitor.purposeVisit %></td>
                                             <td><% visitor.requested_on1 %></td>
                                             <td><% visitor.requested_on %></td>
                                             <td><%visitor.hosteNumber %></td>
                                             <td><%visitor.roomNumber %></td>
                                             
                                             <td >
                                                <div class="dropdown">
                                                   <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                   <span class="caret"></span></button>
                                                   <ul class="dropdown-menu action" id="<% visitor._id %>" >
                                                      <li ng-if="visitor.status !='-2'"   id="-2"  ><a href="javascript:void(0);"  >Class</a></li>
                                                      <li ng-if="visitor.status !='1'"  id="1"><a href="javascript:void(0);" >No Class</a></li>
                                                   </ul>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr ng-hide="visitors.length">
                                             <td colspan="8" align="center"><b>No visitors to display</b></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div >Showing <%visitorsFrom %> to <%visitorsTo%> of <% totalVisitors %> entries</div>
                                       </div>
                                       <div class="col-md-6 text-right">
                                          <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="messages_with_icon_title">
                                 <div class="table-responsive body">
                                    <table class="table table-bordered table-striped table-hover ">
                                       <thead>
                                          <tr>
                                             <th width="5%">Sl No</th>
                                           <!--   <th width="5%">User_id</th> -->
                                             <th width="5%">Roll_No</th>
                                             <th width="5%">Name_Of_The Student</th>
                                             <th>Sex</th>
                                             <th>Email_Id</th>
                                             <th>Mobile_No</th>
                                             <th>Terms Credit</th>
                                             <th>CFR</th>
                                             <!-- 
                                                <th>Request_Date &Time </th>
                                                <th>Placed & Unplaced</th> -->
                                             <th>Place of Visit </th>
                                             <th>Purpose of Visit </th>
                                             <th width="15%"> Request_Date  </th>
                                             <th width="15%"> Return_Date </th>
                                             <!--  <th>Alternate Contact Number</th> -->
                                             <th>Hostel Number</th>
                                             <th>Room Number</th>
                                             <!--   <th>Declaration acceptance by the Student</th>
                                                <th>No of Late Enteries</th> -->
                                             <!--     <th >Status</th> -->
                                           
                                             <th >Action</th>
                                             <!--  <th>More</th> -->
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr ng-repeat="visitor in visitors" id="<% visitor._id %>"   >
                                             <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                           <!--   <td ><% visitor._id %></td> -->
                                             <td><%visitor.requester.rollNo %></td>
                                             <td><%visitor.requester.name %></td>
                                             <td><%visitor.requester.gender %></td>
                                             <td><%visitor.requester.email %></td>
                                             <td><%visitor.requester.mobileNumber %></td>
                                       <td ng-if='visitor.requester.placed=="1"'> <b style="color:red">P</b></td>
                                      <td ng-if='visitor.requester.placed!="1"'><%visitor.requester.termCredits %></td>
                                             <td><%visitor.requester.carryForward %></td>
                                             <td><%visitor.placeOfVisit %></td>
                                             <td><%visitor.purposeVisit %></td>
                                             <td><% visitor.requested_on1 %></td>
                                             <td><% visitor.requested_on %></td>
                                             <td><%visitor.hosteNumber %></td>
                                             <td><%visitor.roomNumber %></td>
                                          <!--   <td>
                                                <div class="dropdown">
                                                   <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                   <span class="caret"></span></button>
                                                   <ul class="dropdown-menu action" id="<% visitor._id %>" >
                                                      <li ng-if="visitor.status !='-2'"    id="-2"  ><a href="javascript:void(0);"  >Class</a></li>
                                                      <li ng-if="visitor.status !='1'"  id="1"><a href="javascript:void(0);" >No Class</a></li>
                                                   </ul>
                                                </div>
                                             </td> -->
                                          </tr>
                                          <tr ng-hide="visitors.length">
                                             <td colspan="15" align="center"><b>No records to display</b></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div >Showing <%visitorsFrom %> to <%visitorsTo%> of <% totalVisitors %> entries</div>
                                       </div>
                                       <div class="col-md-6 text-right">
                                          <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- #END# Tabs With Icon Title -->
            </div>
         </div>
      </div>
   </div>
</section>
<div class="modal fade product_view" id="product_view">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
         </div>
         <div class="modal-body">
         </div>
      </div>
   </div>
</div>
<script src="{{asset('Nightout/ng-controls/Warden_controls.js')}}"></script>
<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
<!-- <script type="text/javascript">
   $('#datepicker').datepicker({
       format: 'dd-mm-yyyy'
   });
   </script> -->
   <script type="text/javascript">
      // alert(window.location.hash);
      $('[href="'+window.location.hash+'"]').trigger('click');
   </script>
@endsection