  @extends('Nightout.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
<section class="content" id="VisitorsController" ng-controller="VisitorsController">
    <div class="container-fluid">
        <div class="block-header">
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header form-inline">
                        <h2>
                             <span>Nightout Request (<%totalVisitors%>)</span>
                            <p class="pull-right"  >
                                <label for="search" class="small-label">Search: </label>
                                <select name="status" id="status" class="form-control show-tick">
                                    <option  >Pending</option>
                                    <option value="2"> Approvred</option>
                                    <option value="-1">Rejected</option>
                                </select>
                                <input type="text" class="form-control" name="search-visitor" title="Type any details" placeholder="Type any details" id="search" >
                                <input type="text" id="datepicker" class="form-control date" value="{{$current_day}}" name="date_of_visit" title="Date of visit" placeholder="Date of visit"  >
                            </p>                         </h2>
                    </div>

                    
                         <div class="body ">
                        <table class="table table-bordered table-striped table-hover">
                   
                            <thead>
                                <tr>
                                   <th width="5%">Sl No</th>
                                            
                                             <th width="5%">Roll_No</th>
                                    <th width="5%">Name_Of_The Student</th>
                                            <th>Sex</th>
                                            
                                            <th>Email_Id</th>
                                            <th>Mobile_No</th>
                                            <th>Terms Credit</th>
                                            <th>CFR</th>
                                            <!-- 
                                            <th>Request_Date &Time </th>
                                            <th>Placed & Unplaced</th> -->
                                             <th>Place of Visit </th>
                                              <th>Purpose of Visit </th>
                                                <th width="15%"> Request_Date  </th>
                                                 <th width="15%"> Return_Date </th>
                                             

                                              <!--  <th>Alternate Contact Number</th> -->
                                               
                                                 <th>Hostel Number</th>
                                                  <th>Room Number</th>
                                                 <!--   <th>Declaration acceptance by the Student</th>
                                                    <th>No of Late Enteries</th> -->
                                               <th  ng-if="showBtns" ng-hide="showSave "  >Approve By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="visitor in visitors" id="<% visitor._id %>">
                                    <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                   
                                    <td><%visitor.requester.rollNo %></td>
                                    <td><%visitor.requester.name %></td>
                                                                     
                                     <td><%visitor.requester.gender %></td>
                                       <td><%visitor.requester.email %></td>
                                      <td><%visitor.requester.mobileNumber %></td>
                                     
                                            <td><%visitor.requester.termCredits %></td>
                                       <td><%visitor.requester.carryForward %></td>

                                       
                                        <td><%visitor.placeOfVisit %></td>
                                     <td><%visitor.purposeVisit %></td>
                                      <td><% visitor.requested_on1 %></td>
                                     <td><% visitor.requested_on %></td>
                                        <td><%visitor.hosteNumber %></td>
                                         <td><%visitor.roomNumber %></td>



                                        <td ng-if="showBtns" ng-hide="showSave " style="color: green" id="name1"  ><b><%visitor.user.username %></b></td>
                                  
                                  
                                </tr>
                                <tr ng-hide="visitors.length">
                                    <td colspan="8" align="center"><b>No visitors to display</b></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <div >Showing <%visitorsFrom%> to <%visitorsTo%> of <%totalVisitors%> entries</div>
                          </div>
                          <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{asset('Superadmin/ng-controls/nightout_controls.js')}}"></script>
<script src="{{asset('Assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script>
  
@endsection