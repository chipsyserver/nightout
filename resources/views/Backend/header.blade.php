<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> Tapmi VMS </title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="/Backend/webadmin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/Assets/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="/Backend/webadmin/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="/Backend/webadmin/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Morris Chart Css-->
    <!-- Custom Css -->
    <link href="/Backend/webadmin/css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/Backend/webadmin/css/themes/all-themes.css" rel="stylesheet" />
    <link href="/Backend/webadmin/css/custom.css" rel="stylesheet" />

     <!-- Jquery Core Js -->
    <script src="/Backend/webadmin/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="/Backend/webadmin/plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Select Plugin Js -->
    <!-- <script src="/Backend/webadmin/plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <!-- Waves Effect Plugin Js -->
    <script src="/Backend/webadmin/plugins/node-waves/waves.js"></script>
    <!-- Custom Js -->
    <script src="/Backend/webadmin/js/admin.js"></script>
    <script src="{{asset('Assets/angular/angular.js')}}"></script>
    <script src="{{asset('Assets/angular/route.js')}}"></script>
    <script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>
    <!-- Demo Js -->
    <script src="/Backend/webadmin/js/demo.js"></script>
    <script src="/Backend/webadmin/js/custom.js"></script>
</head>