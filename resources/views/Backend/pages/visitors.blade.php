@extends('Backend.layout.app')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
<section class="content" ng-controller="VisitorsController">
    <div class="container-fluid">
        <div class="block-header">
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header form-inline">
                        <h2>
                            <span>Visitors (<%totalVisitors%>)</span>
                            <p class="pull-right">
                                <label for="search" class="small-label">Search: </label>
                                <select name="status" class="form-control show-tick">
                                    <option value="">Requested</option>
                                    <option value="1">Visited</option>
                                    <option value="0">Non visited</option>
                                </select>
                                <input type="text" class="form-control" name="search-visitor" title="Type any details" placeholder="Type any details" id="search" >
                                <input type="text" id="datepicker" class="form-control date" value="{{$current_day}}" name="date_of_visit" title="Date of visit" placeholder="Date of visit"  >
                            </p>
                        </h2>
                    </div>
                    <div class="body ">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="10%">Visitor Id</th>
                                    <th width="15%">Name</th>
                                    <th width="10%">Mobile Number</th>
                                    <th width="20%">Address</th>
                                    <th width="10%">Date of Visit</th>
                                    <th width="10%">Requested By</th>
                                    <th width="15%">Requested At</th>
                                    <th width="5%"> Preview </th>
                                    <th width="5%"> Print </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="visitor in visitors">
                                    <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %>
                                       
                                    </th>
                                    <td><% visitor._id %></td>
                                    <td><% visitor.name %></td>
                                    <td><% visitor.mobile_num %> </td>
                                    <td><% visitor.address %> </td>
                                    <td><% visitor.date_of_visit %> </td>
                                    <td ng-if="visitor.requester.username"><% visitor.requester.username %> </td>
                                    <td ng-if="!visitor.requester">
                                        Security 
                                    </td>
                                    <td><% visitor.requested_on %> </td>
                                    <td>
                                        <a href="javascript:void(0);" ng-click="previewCard($index)">
                                            <i class="fa fa-address-card"></i>
                                        </a>
                                    </td>
                                    <td>   <a class="dt-button buttons-print" tabindex="0" aria-controls="DataTables_Table_1" href="/vms/print-pass/<%visitor._id%>" target="_blank"><span>Print</span></a>  </td>
                                </tr>
                                <tr ng-hide="visitors.length">
                                    <td colspan="8" align="center"><b>No visitors to display</b></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <div >Showing <%visitorsFrom%> to <%visitorsTo%> of <%totalVisitors%> entries</div>
                          </div>
                          <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade product_view" id="product_view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="{{asset('Backend/ng-controls/visitors_controls.js')}}"></script>
<script src="{{asset('Assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script>
@endsection