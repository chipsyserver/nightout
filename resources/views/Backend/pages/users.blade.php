@extends('Backend.layout.app')
@section('content')
{{--<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">--}}
<section class="content" ng-controller="UsersController">
    <div class="container-fluid">
        <div class="block-header">
        </div>
        <div class="row clearfix">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header form-inline">
                        <h2>
                            <span>Users (<%totalUsers%>)</span>
                            <p class="pull-right">
                                <label for="search" class="small-label">Search: </label>
                                <input type="text" class="form-control" name="smart_query" title="Type name or mobile no." placeholder="Type name or mobile no.">
                                {{--<input type="text" id="datepicker" class="form-control date" value="{{$current_day}}" name="date_of_visit" title="Date of visit" placeholder="Date of visit"  >--}}
                            </p>
                        </h2>
                    </div>
                    <div class="body ">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="10%">Sl No</th>
                                    <th width="50%">Name</th>
                                    <th width="20%">Mobile Number</th>
                                    <th width="15%">Registered on.</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in users">
                                    <th scope="row"><%(usersPerPage * (currentPage-1)) + $index+1  %></th>
                                    <td><% user.username %></td>
                                    <td ng-if='user.mobileNumber'><% user.mobileNumber %> </td>
                                    <td ng-if='!user.mobileNumber'>-NA-</td>
                                    <td><% user.registered_on %> </td>
                                </tr>
                                <tr ng-hide="users.length">
                                    <td colspan="4" align="center"><b>No users to display</b></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <div >Showing <%usersFrom%> to <%usersTo%> of <%totalUsers%> entries</div>
                          </div>
                          <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade product_view" id="product_view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="{{asset('Backend/ng-controls/users_controls.js')}}"></script>
{{--<script src="{{asset('Assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script>--}}
@endsection