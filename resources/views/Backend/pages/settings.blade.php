@extends('Backend.layout.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
<section class="content" ng-controller="SettingsController">
    <div class="container-fluid">
        <form ng-enter="updateProfile()" name='update_profile'>
        	{{csrf_field()}}
            <div class="block-header">                
            </div>
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h4>
                                Change Username/ Password
                            </h4>
                        </div>
                        <div class="body">
                            <label for="username">Username</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="username" id="username" required="required" name="username" class="form-control" placeholder="Username" value="{{Auth::user()->username}}" data-parsley-errors-container="#username_error">
                                </div>
                                <div id="username_error"></div>
                            </div>
                            <input type="checkbox" id="change_pass" name="change_pass" value="1" class="filled-in">
                            <label for="change_pass">Change Password</label><br />
                            <label for="current_password">Current Password</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="current_password" required="required" name="current_password" class="form-control" placeholder="Current Password" data-parsley-errors-container="#old_pass_error">
                                </div>
                                <div id="old_pass_error"></div>
                            </div>
                            <div name="checkd-chg-pass" >
                            </div>
                            <div class="resp-msg"></div>
                            <button type="button" ng-click="updateProfile()" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script src="{{asset('Backend/ng-controls/settings_controls.js')}}"></script>
<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>

@endsection
