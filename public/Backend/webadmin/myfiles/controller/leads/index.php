<?php
session_start();
ob_start();
require '../../include/route.php'; // define route .
require '../../include/validations.php';
require '../../include/ConnectDB.php';
require '../../include/PassHash.php';

$route = new Route();
//if ($_SERVER['REQUEST_METHOD'] === 'POST') {
//register user
$route->add('/', function() {
	    required_filds(array('fbUrl','email'));
	    require'function/insert.php';
		$db=new insert();
		$res=$db->insert_func();
		if($res==0){
			$response=array();
			$response['status']['code']=0;
			$response['status']['message']="Inserted successfully.";
		
		}else{
			$response['status']['code']=1;
			$response['status']['message']="Something went wrong.";
		}
		die(json_encode($response)); 
		
	
});

$route->add('/get-leads', function() {
	    require'function/display.php';
		$db=new display();
		$res=$db->display_func();
});


$route->add('/user-login', function() {
	    require'function/login.php';
		$db=new login();
		$res=$db->login_func();
});

$route->add('/logout', function() {
	  session_unset(); 
          // destroy the session 
         session_destroy(); 
header('Location:/admin/');
});





$route->submit();
