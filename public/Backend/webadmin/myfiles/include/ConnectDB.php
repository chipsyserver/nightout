<?php
 class  ConnectDB{
	 
	 
    function __construct() {
 		  // constructor
    }
 
    function __destruct() {
       // destructor
    }
 
    public function connectdb() {
		
        require_once 'config_setting.php';
        return  $mysqli = new mysqli(HOST, USER, PASSWORD,DATABASE_NAME);
    }
	
	// genarte refresh tooken
	public function refresh_token() { 
     return uniqid();
 	} 
	//genate unique auth key
	public function authentication() { 
	 $today = date("ymdhsi");
	 $rand = sprintf("%04d", rand(0,9999));
	 $authkey = $today . $rand; 
     return md5($authkey);	
 	} 
	
	 public function generateRandomString($length = 15) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
		return  $randomString;
	 }
	
	
 }