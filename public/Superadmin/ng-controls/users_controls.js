var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("userController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    $scope.showSave = false;
$scope.showBtns = true;


//     $scope.reset = function() {
// $('form[name=user_profile]').get(0).reset();
   
//     }

$scope.profile = function() {
        var form = $('[name=user_profile]');
        var proceed = $('[name=user_profile]').parsley().validate();

        if (proceed) {
           
                            var postdata = {};
                            var data = $('[name=user_profile]').serializeArray();
                            $.each(data, function() {
                                postdata[this.name] = this.value || '';
                            });

                               

                  $.post('/superadmin/user', postdata, function(response) {
                                if (response.success) {
                                    $.alert({
                                        title: 'Success!',
                                        content: 'Details updated',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function() {
                                                    // ('user_profile').reset();
                                                     $('form[name=user_profile]').get(0).reset();
                                                    // window.location='/nightout/pgp-office/request';
                                                    form.find('.resp-msg').html('  ');
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    form.find('.resp-msg').html('* '+display).css('color', 'red');
                                }
                            }, 'json');
                        
        }
    }






    $scope.update = function(event) {
                                
          
           var id=angular.element(event.currentTarget).attr('id');
          
            
      // alert(id);
        var form = $('[name=user_profile]');
        var proceed = $('[name=user_profile]').parsley().validate();

        if (proceed) {
             $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to Update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {

           
                            var postdata = {};
                            var data = $('[name=user_profile]').serializeArray();
                            $.each(data, function() {
                                postdata[this.name] = this.value || '';
                            });

                               

                  $.post('/superadmin/updateuser/'+id, postdata, function(response) {
                                if (response.success) {
                                    $.alert({
                                        title: 'Success!',
                                        content: 'Details updated',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function() {
                                                    // ('user_profile').reset();
                                                      
                                                     $('form[name=user_profile]').get(0).reset();

                                                    // window.location='/nightout/pgp-office/request';
                                                    // form.find('.resp-msg').html('  ');
                                                      // $('[name="update"]').empty();
                             $scope.showSave=false;
                            
                           
                              $scope.getUsers(1);

                                                }
                                            }
                                        }
                                    });
                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    form.find('.resp-msg').html('* '+display).css('color', 'red');
                                }
                            }, 'json');
                        }
                                    },
                                    cancel: function() {}
                                }
                            });
                        
        }
    }



            $scope.delete = function(event){
                // alert(event.target.id);
                var id=event.target.id;
   


                 
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to Delete details?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-red',
                        action: function() {

                                                    
                  
    
                     $http.post('/superadmin/delete',{     
                        'id':id }).then(function(response){
                            
                                
                                  $.alert({
                                                        title:" ",
                                                        content: "Successfully Deleted",
                                                        // icon: 'fa fa-rocket',
                                                        animation: 'zoom',
                                                        closeAnimation: 'zoom',
                                                        buttons: {
                                                            okay: {
                                                                text: 'Okay',
                                                                btnClass: 'btn-green',
                                                                action: function() {
                                                                     $scope.getUsers(1);
                                                                    
                                                                }
                                                            }
                                                        }
                                                    });
                             
                        });
                        
                            


                                             
                                        }
                                    },
                                    cancel: function() {}
                                }
                            });
                        
                  
                }


 $scope.edit = function(event){
                // alert(event.target.id);
               
                  var id=angular.element(event.currentTarget).attr('id');

                $http.get('/superadmin/edituser/'+id) 
                .then(function(res) {
                    var id =res.data._id;
                                  
                  
                     $('#email').val(res.data.email);
                       $('#confirm_password').val(res.data.password);
                        $('#new_password').val(res.data.password);
                    
                    

                   
                  if(res.data.sex==1)
                  {
                   $("#radio_1").prop("checked", true);
                  }
                  else
                  {
                   $("#radio_2").prop("checked", true);
                  }
                  
                    $('#username').val(res.data.username);
                    $('#empid').val(res.data.empid);
                   
                     // var change_pass = '<button type="button" id='+res.data._id +' name="update"  class="btn btn-primary waves-effect update">Update</button>';
                           
                           
                     //         $('[name="update"]').empty().html(change_pass);
                           
      $scope.showSave=true;

$('.updatewarden').attr('id',id);

            });
        }



    // Pagination of Users
    $scope.maxSize = 3;
    $scope.$watch("currentPage", function() {
        
        $scope.getUsers($scope.currentPage);
    });

    //load Users     
    $scope.getUsers = function(page=1) {
        $http.get('/superadmin/userdetails?page='+page)
            .then(function(res) {
                aler(page);
                $scope.users = res.data.data;

                $scope.totalUsers = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.usersPerPage = res.data.per_page;
                $scope.usersFrom = res.data.from ? res.data.from : 0;
                $scope.usersTo = res.data.to ? res.data.to : 0;
        });
    }
}]);

