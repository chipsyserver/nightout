var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    
     $(document).on('change', '[name=status]', function(e) {
      
 
     
          var data= $('[name=status]').val();
          // date_of_visit: $('[name=date_of_visit]').val(),
         
        
     
       
        $scope.getVisitors(1,data);
      });
 

      $scope.maxSize = 7;
    $scope.$watch("currentPage", function() {
                
        $scope.getVisitors($scope.currentPage);
    });
    //load Visitors     
    $scope.getVisitors = function(page=1,data='data') {
        $http.get('/superadmin/display/'+data+'?page='+page)
            .then(function(res) {

             
               
                $scope.visitors = res.data.data;


                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }
 
    
}]);
