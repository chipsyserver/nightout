var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
      
$scope.showBtns = false;
    $(document).on('keyup', '[name=search-visitor]', function(e) {
        // var code = (e.keyCode ? e.keyCode : e.which);
        // if (code == 13 || $(this).val()) {

         // alert("hii");
         // alert($('[name=search-visitor]').val());
            var myObject = {
              status: $('[name=status]').val(),
              // date_of_visit: $('[name=date_of_visit]').val(),
              smart_query: $('[name=search-visitor]').val()
            };

            
            var cond = $.param( myObject );
            var data ="data";

            $scope.getVisitors(1, cond,data);
        // }
    });
    
    $(document).on('change', '[name=date_of_visit]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        // if (code == 13 || $(this).val()) {
            var myObject = {
              // status: $('[name=status]').val(),
              date_of_visit: $('[name=date_of_visit]').val(),
              smart_query: $('[name=search-visitor]').val()
            };
            
            var cond = $.param( myObject );
            var data ="data";
            $scope.getVisitors(1, cond,data);
        // }
    });

    $(document).on('change', '[name=status]', function(e) {
      
     if($('[name=status]').val()=="Pending")
     {
    
       var data ="pending";
       $scope.showBtns = false;
        $scope.getVisitors(1, cond,data);


     }
     else
     {

        var myObject = {
          status: $('[name=status]').val(),
          // date_of_visit: $('[name=date_of_visit]').val(),
          smart_query: $('[name=search-visitor]').val()
        };
        
        var cond = $.param( myObject );
        var data ="data";
       $scope.showBtns = true;
        $scope.getVisitors(1, cond,data);

      }
    });



var url=window.location.hash;

  if(url=="#approved")
      {
        $('#status').val('2');
         var data="data"
      }
      else if (url=="#reject") {
         var data="data"
        $('#status').val('-1');
      }
      


    // Pagination of Visitors
    $scope.maxSize = 3;
    $scope.$watch("currentPage", function() {
        var myObject = {
          status: $('[name=status]').val(),
          // date_of_visit: $('[name=date_of_visit]').val(),
          smart_query: $('[name=search-visitor]').val()
        };



        var cond = $.param( myObject );
        // var data="pending";
        $scope.getVisitors($scope.currentPage, cond,data);
    });

    //load Visitors     
    $scope.getVisitors = function(page=1, $cond='',data='pending') {

    
        $http.get('/superadmin/nightout/'+data+'?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                
                $scope.visitors = res.data.data;

                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }

   
}]);