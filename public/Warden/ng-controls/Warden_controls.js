var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    
      $(document).on('click', 'ul.status > li', function() {
      
      var data = $(this).attr('id');

       
       
       if(data=='0')
       {
        $scope.getVisitors(1);
       }
       else
       {

        var pgp=$('[name=pgp]').val();
        

        $scope.getVisitors(1,data,pgp);   

        
     }
    }); 
      $(document).on('change', '[name=pgp]', function(e) {
      
      
     if($('[name=status]').val()=="0")
     {
    
       var pgp ='0';
       
        $scope.getVisitors(1, pgp);

  alert($('[name=pgp]').val()); 
     }
     else
     {

        var pgp=$('[name=pgp]').val();
        
         var data = $('ul.status > li.active').attr('id');
        $scope.getVisitors(1,data,pgp);
       

      }
    });

     
 $(document).on('click', ' ul.action > li', function() {
                 
   

          var userid=$('.userid').attr('id');
             var action = $(this).attr('id');
            var id= $(this).closest('tr').attr('id');
            var ths=$(this);
            var txt="Approved";
         if(action==-1){
          txt="Reject";
         }
       
          
            $.confirm({
    title: 'Reson for '+txt,
     type: 'red',
    content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
       '<textarea type="text" placeholder="Comment" class="name form-control" required />' +
    '</div>' +
    '</form>',
    buttons: {
        formSubmit: {
            text: 'Submit',
            btnClass: 'btn-blue',
            action: function () {
                var name = this.$content.find('.name').val();
                if(!name){
                    $.alert('provide a valid name');
                    return false;
                }
                // $.alert('Your name is ' + name);
                 $http.post('/warden/action',{
        'action':action,
        'userid':userid,
        'comment':name,
        'id':id }).then(function(response){
             var id=response.data.id;
             var action=response.data.action;

    
       $.alert({
                                        title: 'Success!',
                                        content: 'Details Updated',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-Green',
                                                action: function() {
                                            ths.closest("tr#"+id).hide();
                                                      
                                                 

                                                   
                                                }
                                            }
                                        }
                                    });
             
    
             
        });
            }
        },
        cancel: function () {
            //close
        },
    },
   
});


    
 
        
   });

    $scope.getVisitors = function(cond=''){
        

        $http.get('/nightout/pgp-office/display/status'+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                 $scope.visitors = res.data.data;
                 // alert($scope.visitors);


            });
        }
   
    // Pagination of Visitors
    $scope.maxSize = 7;
    
    $scope.$watch("currentPage", function() {
       var data = $('ul.status > li.active').attr('id');
      // alert(cond);
                 // var cond= $('[name=pgp]').val();
         
        $scope.getVisitors($scope.currentPage,data);
    });

    //load Visitors     
    $scope.getVisitors = function(page=1,data='0',pgp='') {
     
        $http.get('/warden/nightout/'+data+'/'+pgp+'?page='+page)
            .then(function(res) {

                
               
                $scope.visitors = res.data.data;

                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }
 
    
}]);
