var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    


//  $("[name=import]").click(function() {


// var extension = $('#fileUpload').val().split('.').pop().toLowerCase();

//           if ($.inArray(extension, ['png', 'jpg']) == -1) {
//              $.alert({
//                      title: 'We only use .xls only',
//                      content: 'file extension is error',
//                      });
//              proceed = false;

                    
//               }
              
//           });


          


//             if (proceed) {




//                 var postdata = new FormData();

//                 postdata.append("import_file", $('input[name=import_file]')[0].files[0]);

//                 // var form = new FormData();   
//                 //    form.append("import_file",$("#import")[0].files[0]);




//                 $.ajax({
//                     url: "../importexcel",
//                     async: !1,
//                     type: "POST",
//                     dataType: "json",
//                     contentType: false, // The content type used when sending data to the server.
//                     cache: false, // To unable request pages to be cached
//                     processData: false,
//                     data: postdata,
//                     success: function(response) {


                        



//                         if (response.success) {
                           

//                             var display = response.message;
//                             $.alert({
//                             title: 'this',
//                             content:  display,
//                              });
//                          //   $('.message_area1').html(display).css('color', 'green');
//                             //  data1=response.data1;
//                             //  $('#value1').html(data1).css('color','blue');
//                             // window.location="/company/create";

//                         } else {
//                             var display = "";
//                             if ((typeof response.message) == 'object') {
//                                 $.each(response.message, function(key, value) {
//                                     display = value[0]; //fetch oth(first) error.
//                                 });
//                             } else {
//                                 // alert(JSON.stringify(response));


//                                 display = response.message;
//                                  }
//                           $.alert({
//                             title: 'Error message ',
//                             content:  display,
//                              });
//                             // $('.position').html(position).css('color', 'red');

//                             // $('.number').html(number).css('color', 'red');
//                             // $('.message_area1').html(display).css('color', 'red');

//                             //   $('[name=from_company] input').val('');

                            
//                         }
//                     }
//                 });

// }
      
 



    // Pagination of Visitors
    $scope.maxSize = 7;
    $scope.$watch("currentPage", function() {
                
        $scope.getVisitors($scope.currentPage);
    });

    //load Visitors     
    $scope.getVisitors = function(page=1) {
        $http.get('/office/display?page='+page)
            .then(function(res) {

                
               
                $scope.visitors = res.data.data;

                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }

    
}]);
