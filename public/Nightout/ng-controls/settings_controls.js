
var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});
myApp.controller("SettingsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    $(document).on('keydown', '.form-control', function() {
        $(this).parents('.contentpanel').find('.clear-fix').html('');
        $('.resp-msg').empty();
    });


    $(document).on('change', '[name="change_pass"]', function() {
        if ($(this).is(':checked')) {
            var change_pass = '<label for="new_password">New Password</label>'
                            +'<div class="form-group">'
                            +'<div class="form-line">'
                            +'<input type="password" required="required" name="new_password" id="new_password" class="form-control" placeholder="New Password" data-parsley-errors-container="#new_pass_error">'
                            +'</div>'
                            +'<div id="new_pass_error"></div>'
                            +'</div>'
                            +'<label for="confirm_password">Confirm Password</label>'
                            +'<div class="form-group">'
                            +'<div class="form-line">'
                            +'<input type="password" required="required" data-parsley-equalto="#new_password" data-parsley-equalto-message="Confirm Password should match the New Password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Repeat Password" data-parsley-errors-container="#rep_pass_error">'
                            +'</div>'
                            +'<div id="rep_pass_error"></div>'
                            +'</div>';

            $('[name="checkd-chg-pass"]').empty().html(change_pass);
        } else {
            $('[name="checkd-chg-pass"]').empty();
        }
    });

    $scope.updateProfile = function() {
        var form = $('[name=update_profile]');
        var proceed = $('[name=update_profile]').parsley().validate();

        if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            var postdata = {};
                            var data = $('[name=update_profile]').serializeArray();
                            $.each(data, function() {
                                postdata[this.name] = this.value || '';
                            });
                               
                               var url="";
                            if(custom_url=="nightout"){
                             url='/nightout/pgp-office/sub-settings';
                            }
                            else if(custom_url=="superadmin") {
                                 url='/superadmin/sub-settings';
                            }
                            else if(custom_url=="office"){
                                url='/office/sub-settings'; 
                            }
                            else{
                                url='/warden/sub-settings'; 
                            }

                            $.post(url, postdata, function(response) {
                                if (response.success) {
                                    $.alert({
                                        title: 'Success!',
                                        content: 'Details updated',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function() {
                                                     $.alert({
                                        title: 'Success!',
                                        content: 'Password Changes success',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function() {
                                                    $('#change_pass').prop('checked', false);
                                                     $('[name="checkd-chg-pass"]').empty();
                                                     $('[name="current_password"]').val('');
                                                    // window.location='/nightout/pgp-office/request';
                                                }
                                            }
                                        }
                                    });
                                                    // window.location='/nightout/pgp-office/request';
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    form.find('.resp-msg').html('* '+display).css('color', 'red');
                                }
                            }, 'json');
                        }
                    },
                    cancel: function() {}
                }
            });
        }
    }
}]);

myApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keypress", function(e) {
            if (e.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, {
                        'e': e
                    });
                });
                e.preventDefault();
            }
        });
    };
});