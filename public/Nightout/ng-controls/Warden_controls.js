var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    
      $(document).on('click', 'ul.status > li', function() {
      
      var data = $(this).attr('id');

       
       
       if(data=='0')
       {
         var pgp=$('[name=pgp]').val();
        $scope.getVisitors(1,0,pgp);
       }
       else
       {
        // alert(data);
        // var data=$('[name=status ]').val();
        
 var pgp=$('[name=pgp]').val();
        $scope.getVisitors(1,data,pgp);   

        
     }
    }); 
      $(document).on('change', '[name=pgp]', function(e) {
      
      
     if($('[name=pgp]').val()=="0")
     {
    
       var pgp ='0';
        var data = $('ul.status > li.active').attr('id');
        $scope.getVisitors(1,data,pgp);

  // alert($('[name=pgp]').val()); 
     }
     else
     {

        var pgp=$('[name=pgp]').val();
        
         var data = $('ul.status > li.active').attr('id');
        $scope.getVisitors(1,data,pgp);
       

      }
    });

     
 $(document).on('click', ' ul.action > li', function() {
                 
   

          var userid=$('.userid').attr('id');
             var action = $(this).attr('id');
            var id= $(this).closest('tr').attr('id');
            var ths=$(this);

        // if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {

                                                    
                  
    
     $http.post('/nightout/pgp-office/action',{
        'action':action,
        'id':id }).then(function(response){
             var id=response.data.id;
             var action=response.data.action;

    
       $.alert({
                                        title: 'Success!',
                                        content: 'Details Updated',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-Green',
                                                action: function() {
                                            ths.closest("tr#"+id).hide();
                                                      
                                                 

                                                   
                                                }
                                            }
                                        }
                                    });
             
    
             
        });
        
            


                             
                        }
                    },
                    cancel: function() {}
                }
            });
        
   });

    $scope.getVisitors = function(cond=''){
        

        $http.get('/nightout/pgp-office/display/status'+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                 $scope.visitors = res.data.data;
                 // alert($scope.visitors);


            });
        }
   
    // Pagination of Visitors
    $scope.maxSize = 7;
    
    $scope.$watch("currentPage", function() {
       var data = $('ul.status > li.active').attr('id');
     
      // alert(cond);
                 // var cond= $('[name=pgp]').val();
         
        $scope.getVisitors($scope.currentPage,data,0);
    });

    //load Visitors     
    $scope.getVisitors = function(page=1,data='',pgp='') {
     
        $http.get('/nightout/pgp-office/request/'+data+'/'+pgp+'?page='+page)
            .then(function(res) {

                
               
                $scope.visitors = res.data.data;

                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }
 
    
}]);
