var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {

 $(document).on('click', ' ul.status > li', function() {
                 
   


  
        var status = $(this).attr('id');
          var id= $(this).closest('tr').attr('id');
    var ths=$(this);

        // if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {

                                                    
                  
    
     $http.post('/nightout/pgp-office/status',{
        'status':status,
        'id':id }).then(function(response){
             var id=response.data.id;
             var status=response.data.status;
             ths.closest("tr#"+id).hide();
             
              });
        
                                         
                        }
                    },
                    cancel: function() {}
                }
            });
        
   });

    $scope.getVisitors = function(cond=''){
        

        $http.get('/nightout/pgp-office/display/status'+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                 $scope.visitors = res.data.data;
                 // alert($scope.visitors);


            });
        }



        $(document).on('change', '[name=pgp]', function(e) {
      
    
     
          var data= $('[name=pgp]').val();
              
     
       
        $scope.getVisitors(1,data);
      });

    // Pagination of Visitors
    $scope.maxSize = 7;
    $scope.$watch("currentPage", function() {
                 var data= $('[name=pgp]').val();

        $scope.getVisitors($scope.currentPage,data);
    });

    //load Visitors     
    $scope.getVisitors = function(page=1,data="data") {
        $http.get('/nightout/pgp-office/display/'+data+'?page='+page)
            .then(function(res) {

                
               
                $scope.visitors = res.data.data;

                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }

    
}]);
