<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class RequestNightOut extends Model
{
    protected $collection = 'request_nightout';
    
     public function requester()
    {
        return $this->belongsTo('App\PgStudents', 'user_id');
    }

      public function user()
    {
        return $this->belongsTo('App\User', 'warden_id');
    }
   
}
