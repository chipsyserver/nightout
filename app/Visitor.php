<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class Visitor extends Model
{
    protected $collection = 'visitors';
    
    public function requester()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
