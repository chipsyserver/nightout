<?php
namespace App\Repository\Backend\Post;
use DB;
use App\Http\Controllers\Controller;
use Image;
class Insert extends Controller {
	  public static function insert_func($data){
		  
	   $dateTime=date('Y-m-d H:i:s');
	   $res=DB::collection('admin_post')->insertGetId([
					'categoryId'            => $data['categoryId'],
					'post_details'          => $data['post_details'],
					'addedTime'             => $dateTime,
					'updatedTime'           => $dateTime,
					]);
					
	   if(isset($data['image_file'])) {
	   $photo=$data['image_file'];
	   	
	   $imagecategory = "admin_post";
	   $dirYear = date("Y");
	   $dirMonth = date("m");
	   $imgName = uniqid();
	   $imgTime = time();
	   if (!is_dir(app_path() . "/Images")) {  mkdir(app_path() . "/Images");  } 
	   if (!is_dir(app_path() . "/Images/" . $imagecategory)) {  mkdir(app_path() . "/Images/" . $imagecategory);  } 
	   if (!is_dir(app_path() . "/Images/" . $imagecategory . "/" . $dirYear)) {  mkdir(app_path() . "/Images/" . $imagecategory . "/" . $dirYear);  } 
	   if (!is_dir(app_path() . "/Images/" . $imagecategory . "/" . $dirYear . "/" . $dirMonth)) {  mkdir(app_path() . "/Images/" . $imagecategory . "/" . $dirYear. "/" . $dirMonth);  }
		list($width, $height) = getimagesize($photo); 
		$ratio = $width/$height; // width/height
		if( $ratio > 1) {$width = 300;$height = 300/$ratio;}else {$width = 300*$ratio;$height = 300;}
	    $ext = $photo->getClientOriginalExtension(); 
	    $smallimage = '/Images/' . $imagecategory . "/" . $dirYear . "/" . $dirMonth . "/" .  $dirYear . "_" . $dirMonth . "_" . $imgTime . "_" . $imgName; 
        Image::make($photo)->resize($width, $height)->save(app_path() . $smallimage . "_s." . $ext);
	    if( $ratio > 1) { $width = 800; $height = 800/$ratio; }else { $width = 800*$ratio; $height = 800; }
	    Image::make($photo)->resize($width, $height)->save(app_path() . $smallimage . "_l." . $ext);
			
		     	
				 
				 DB::collection('admin_post_images')->insertGetId([
					'admin_post_id'            => $res,
					'image_path'               =>app_path().$smallimage,
					'image_ext'                =>$ext
					
					]);	
			}		
			$response['status']['code']     =0; 
			$response['status']['message']  ='Inserted successfully.';
			return $response; 
    	}  
		
}
