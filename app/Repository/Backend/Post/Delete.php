<?php
namespace App\Repository\Backend\Post;

use DB;
use App\Http\Controllers\Controller;

class Delete extends Controller
{
    public static function delete_func($id)
    {
        $res=DB::collection('admin_post')->where('_id', $id)->delete();
        DB::collection('admin_post_images')->where('admin_post_id', $id)->delete();
                
        $response['status']['code']     =0;
        $response['status']['message']  ='Deleted successfully.';
        return $response;
    }
}
