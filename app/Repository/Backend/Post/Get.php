<?php
namespace App\Repository\Backend\Post;
use DB;
use App\Http\Controllers\Controller;
use Image;
class Get extends Controller {
	  public static function show_func(){
		  $res=DB::collection('posts')->orderby('_id','DESC')->paginate(50);
		  $cat="";
		  $response['post']['data']=array();
		   foreach($res as $data) {  
			 switch($data['type']) {
			 case 1: $cat='News'; break;
			 case 100: $cat='Job'; break;
			 case 101: $cat='Internship'; break;
			 }
			 
			
			   $data['_id']=(string)$data['_id'];
			   $data['category'] = $cat;
			   $data['post_details'] =strlen($data['post_details']) > 100 ? substr($data['post_details'],0,100) . "..." 
			   :strip_tags($data['post_details']);
			   
			   array_push($response['post']['data'],$data); 
			   
		   }
		   return $response['post']['data'];
	   
      }  
      
      public static function show_func(){
		  $res=DB::collection('admin_post')->orderby('_id','DESC')->paginate(50);
		  $cat="";
		  $response['post']['data']=array();
		   foreach($res as $data) {  
			 switch($data['categoryId']) {
			 case 101: $cat='News'; break;
			 case 102: $cat='Job'; break;
			 case 103: $cat='Internship'; break;
			 }
			 
			 //die(json_encode((string)$data['_id']));
			   $data['_id']=(string)$data['_id'];
			   $data['category'] = $cat;
			   $data['post_details'] =strlen($data['post_details']) > 100 ? substr($data['post_details'],0,100) . "..." 
			   :strip_tags($data['post_details']);
			   
			   array_push($response['post']['data'],$data); 
			   
		   }
		   return $response['post']['data'];
	   
      } 
		
}
