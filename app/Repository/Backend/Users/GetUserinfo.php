<?php
namespace App\Repository\Backend\Users;

use DB;
use App\Http\Controllers\Controller;

class GetUserinfo extends Controller
{
    public static function get_userinfo_func()
    {
        $response =DB::collection('users')
              ->where('roll_id', '!=', 100)
              ->where('active_state', 0)->paginate(10);
        return $response;
    }
    public static function get_user_func($id)
    {
        // die(json_encode((int)$id));
          $response =DB::collection('users')
              ->where('active_state', (int)$id)
              ->paginate(10);
        return $response;
    }
}
