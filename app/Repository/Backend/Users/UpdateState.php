<?php
namespace App\Repository\Backend\Users;
use DB;
use App\Http\Controllers\Controller;
class UpdateState extends Controller {
	    public static function get_userinfo_func($data,$id){
		 $users = DB::collection('users')
               	 ->where('_id',$data['userId'])
                 ->update(['active_state'=>(int)$id ]);
                 
			 $response['status']['code']=0; 
			 $response['status']['message']= 'updated successfully.';
	         return $response; 
		 
		}  
		
}
