<?php
namespace App\Repository\Frontend\Auth;

use DB;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class Login extends Controller
{
    public static function signin_func($data)
    {
        $userlog = DB::collection('users')->where('email', $data['login_email'])
                   ->Where('roll_id', '!=', 100)->select('id', 'password', 'meta', 'first_name', 'last_name')->first();
                   
        if ($userlog) {
            if (Hash::check($data['login_password'], $userlog['password'])) {
                Auth::attempt(array(
                    'userName' => $data['login_email'],
                    'password' => $data['login_password'],
                    'remember_token' =>(isset($data['rememberme']))? true : false,
                    )
                );

                Auth::loginUsingId($userlog['_id']);
                return response()->json(array(
                    'success' => true,
                    'message' => "Login successfully completed",
                    'redirect' => (json_decode($userlog['meta'], true)['first_login']) ? '/add-profile' : ('/'.strtolower($userlog['first_name']).'-'.strtolower($userlog['last_name']))
                ));
            }
        }
        return response()->json(array(
            'success' => false,
            'message' => "Invalid user name or password"
        ));
    }
    public static function signout_func()
    {
        DB::collection('users')->where('_id', Auth::user()->_id)
                   ->update(['last_signin'=>time()]);
        Auth::logout();


        return redirect('/login');
    }
}
