<?php
namespace App\Repository\Frontend\Profile;

use DB;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Hash;

class Update extends Controller
{
    public static function update_profile_func($data)
    {
        $userid=Auth::user()->_id;

        $data['dob'] = strtotime($data['dob']);

        $data['meta']['first_login'] = 0;
        $data['want_to_be'] = implode(',', isset($data['want_to_be']) ? $data['want_to_be'] : []);
        $data['meta'] = json_encode($data['meta']);

        // Likedin  etra info
        DB::collection('users')->where('_id', $userid)->update([
                'mobile_number'=> $data['mobile_number'],
                'gender'=> $data['gender'],
                'dob'=> $data['dob'],
                'address'=> $data['address'],
                'want_to_be'=> $data['want_to_be'],
                'user_type_id'=> $data['user_type'],
                'meta'=> $data['meta']
            ]
        );
        
        DB::collection('work_details')->where('userId', $userid)->delete();

        if (isset($data['workinfo'])) {
            foreach ($data['workinfo'] as $info) {
                DB::collection('work_details')->insertGetId([
                        'userId'            => $userid,
                        'companyName'       => $info['companyName'],
                        'position'          => $info['position'],
                        'city'              => $info['cityName'],
                        'startingTime'      => $info['startingTime'],
                        'endingTime'        => $info['endingTime'],
                    ]
                );
            }
        }
            
        DB::collection('education_details')->where('userId', $userid)->delete();

        if (isset($data['educationinfo'])) {
            foreach ($data['educationinfo'] as $info) {
                DB::collection('education_details')->insertGetId([
                        'userId'            => $userid,
                        'university'        => $info['university'],
                        'startingTime'      => $info['startingTime'],
                        'endingTime'        => $info['endingTime']
                    ]
                );
            }
        }
        
        DB::collection('skill_details')->where('userId', $userid)->delete();

        if (isset($data['skillset'])) {
            $data = explode(',', $data['skillset']);
            foreach ($data as $skill) {
                if ($skill != "") {
                    DB::collection('skill_details')->insertGetId([
                        'userId'            => $userid,
                        'skill'             => $skill,
                      ]
                  );
                }
            }
        }

        $response['status']['code']=0;
        $response['status']['message']= 'updated successfully.';
        return $response;
    }

    public static function change_pass_func($data)
    {
        $n_password = Hash::make($data['newPass']);
        $res = false;

        if (Hash::check($data['oldPass'], Auth::user()->password)) {
            // Likedin  etra info
            $res = DB::collection('users')->where('_id', Auth::user()->_id)->update([
                    'password'=> $n_password,
                ]
            );
        }

        if ($res) {
            $response['status']['code']=0;
            $response['status']['message']= 'updated successfully.';
            Auth::logout();

            return $response;
        } else {
            $response['status']['code']=1;
            $response['status']['message']= 'Current Password is Wrong.';
            return $response;
        }
    }
}
