<?php

namespace App\Repository;

use App\Http\Controllers\Controller;
use App;
use Image;

class ImageUpload extends Controller
{
    private $src;
    private $data;
    private $dst;
    private $dstUrl;
    private $type;
    private $extension;
    private $msg;
    private $avName;

    public function __construct($data, $file, $dest="/uploads/", $action='crop')
    {
        $this->avName = date('YmdHis');
        $this->dst = public_path($dest);
        $this->dstUrl = App::make('url')->to($dest).'/';

        $this->setData($data);
        $this->setFile($file);

        $this->$action($this->src, $this->dst, $this->data);
    }

    private function setData($data)
    {
        if (!empty($data)) {
            $this->data = json_decode(stripslashes($data), true);
        }
    }

    private function setFile($file)
    {
        $errorCode = $file['error'];

        if ($errorCode === UPLOAD_ERR_OK) {
            $this->type = exif_imagetype($file['tmp_name']);

            if (in_array($this->type, [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG])) {
                $this->extension = image_type_to_extension($this->type);
                $this->src       = $this->dst .$this->avName . '.original' . $this->extension;

                if (file_exists($this->src)) {
                    unlink($this->src);
                }

                $result = move_uploaded_file($file['tmp_name'], $this->src);

                if (!$result) {
                    $this->msg = 'Failed to save file';
                }
            } else {
                $this->msg = 'Please upload image with the following types: JPG, PNG, GIF';
            }
        } else {
            $this->msg = 'File upload error!';
        }
    }

    private function crop($src, $dst, $data)
    {
        if (!empty($src) && !empty($dst) && !empty($data)) {
            // open file a image resource
            $img = Image::make($src);
            $data = array_map('round', $data);

            // crop image
            $img->crop($data['width'], $data['height'], $data['x'], $data['y'])->save($dst.$this->avName.'.png');

            unlink($src);
        }
    }

    private function resize($src, $dst, $data)
    {
        if (!empty($src) && !empty($dst) && !empty($data)) {
            // open file a image resource
            $img = Image::make($src);
            $data = array_map('round', $data);

            // $img->resize($data['width'], $data['height'])->save($dst.$this->avName.'.png');
            // crop image
            $img->resize($data['width'], $data['height'], function ($constraint) {
                // $constraint->aspectRatio();
                // $constraint->upsize();
            })->save($dst.$this->avName.'.png');

            unlink($src);
        }
    }

    public function getResult()
    {
        $avatar = $this->avName.'.png';
        return [
            'avName' => $avatar? : '',
            'url' => $avatar ? $this->dstUrl.$avatar : '',
        ];
    }

    public function getMsg()
    {
        return $this->msg;
    }
}
