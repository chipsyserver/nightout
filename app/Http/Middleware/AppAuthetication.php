<?php 
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
require_once app_path().'/Repository/Crypt.php';
class AppAuthetication
{

    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $headers = apache_request_headers();
         if (isset($headers['Authorizations'])) {
            $crypt = new \Crypt();
            $crypt->setKey('eff99cfe6876008c6a6e080e4a382be1');
            $crypt->setComplexTypes(true);
            $crypt->setData($headers['Authorizations']);
            try {
                $decrypted = $crypt->decrypt();
                $GLOBALS['userId']=$decrypted['userId'];
            } catch (Exception $e) {
                $response['status']['code'] = 1024;
                $response['status']['message'] = "User not authorized";
                return response()->json($response);
            }
        
        }else {

            $response['status']["code"] = 1023;
            $response['status']["message"] = 'Authorizations not found.';
            return response()->json($response);
        }


        return $next($request);
    }
}
