<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\URL;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class nightout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    //  public function __construct(Guard $nightout)
    // {
    //     $this->nightout = $nightout;
    // }


     public function handle($request, Closure $next)
    {   
        // $user = Auth::guard('nightout')->user();
       // die(json_encode(Auth::guard('nightout')->user()->roleId==1002));
       // if(Auth::check()){
         if (Auth::guard('nightout')->check()) {
       
          if (Auth::guard('nightout')->user()->roleId==1002) {

            return $next($request);
        }
               
            } 
            else{

                 return redirect('/nightout/pgp-office/login');

                  }
        
        
    }
}
