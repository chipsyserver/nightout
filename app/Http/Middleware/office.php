<?php 
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class office
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   

     public function handle($request, Closure $next)
    {   
       //   $auth=Auth::guard('superadmin');
       // if(Auth::check()){
        // die(json_encode(Auth::user()));
      if (Auth::guard('office')->check()) {
          if (Auth::guard('office')->user()->roleId==1004) {

            return $next($request);

               
            } 
        }
            else{

                 return redirect('/office/login');

                  }
        
        
    }
}
