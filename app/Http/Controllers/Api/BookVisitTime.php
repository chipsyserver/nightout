<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Image;
use Config;
use Exception;
use App\Visitor;
use App\Repository\Uploadimage;

require_once app_path().'/Repository/Crypt.php';

class BookVisitTime extends Controller
{
    public function bookvisit_func(Request $request)
    {
        $headers = apache_request_headers();

        if (isset($headers['Authorizations'])) {
            if ($headers['Authorizations']!='security') {
                $crypt = new \Crypt();
                $crypt->setKey('eff99cfe6876008c6a6e080e4a382be1');
                $crypt->setComplexTypes(true);
                $crypt->setData($headers['Authorizations']);
            
                try {
                    $decrypted = $crypt->decrypt();
                } catch (Exception $e) {
                    $response['status']['code'] = 1024;
                    $response['status']['message'] = "User not authorized";

                    return response()->json($response);
                }
            } else {
                $decrypted['userId']="2000";
            }
        }

        $rules = [
          'name'=>'required',
          'mobile_num'=>'required',
          'address'=>'required',
          'whom_to_meet'=>'required',
          'purpose'=>'required' ,
          'date_of_visit'=>'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        $data = $request->all();
      
        if ($validator->fails()) {
            $response['status']['code']=1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }
      

        $visitor = new Visitor;
        $visitor->user_id = (isset($decrypted['userId'])) ? $decrypted['userId'] : '';
        $visitor->name = $data['name'];
        $visitor->mobile_num = $data['mobile_num'];
        $visitor->address = $data['address'];
        $visitor->whom_to_meet = $data['whom_to_meet'];
        $visitor->purpose = $data['purpose'];
        $visitor->date_of_visit = $data['date_of_visit'];
        $visitor->dov_date = strtotime($data['date_of_visit'].' 23:59:59');
        $visitor->status = 0;
        $visitor->visitor_img = '';
        $visitor->visitor_id_img = '';

        $res = $visitor->save();
        $data['userId']=$visitor->_id;
        $response['status']['code']            = 0;
        $response['status']['message']         = "Booked visting successfully.";
        $response['data'] =$data;

        return response()->json($response);
    }

    
    public function bookedtoday_func(Request $request)
    {
        $visitors = Visitor::where('date_of_visit', date('d-m-Y'))->orderBy('_id', 'DESC')->get()->toArray();

        $data['info'] = [];
        foreach ($visitors as &$val) {
            unset($val['user_id']);

            array_push($data['info'], $val);
        }

        $response['status']['code']            = 0;
        $response['status']['message']         = "Booked visting successfully.";
        $response['data'] = $data['info'];
        return response()->json($response);
    }


    public function upload_visitor_pic_func(Request $request)
    {
        $headers = apache_request_headers();
        if (isset($headers['userId'])) {
            $userId= $headers['userId'];
        } else {
            $response['status']['code']=1;
            $response['status']['message']="userId missing";
            return response()->json($response);
        }
        
        $rules = array('visitor_img' => 'required|image|mimes:jpg,jpeg,png');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }

        $img = $request->file('visitor_img');
           
        $visitor_pic=Uploadimage::upload_func($userId, 'visitorphotos', $img);

        $visitor = Visitor::find($userId);

        $visitor->status = 1;
        $visitor->visitor_img = $visitor_pic;

        $res = $visitor->save();

        $response['status']["code"] = 0;
        $response['status']['message']= 'Uploaded successfully.';
        $response['data']['userId']= $userId;
        $response['data']['visitor_img']= $visitor_pic;

        return response()->json($response);
    }
    
    
    public function upload_visitor_id_pic_func(Request $request)
    {
        $headers = apache_request_headers();
        if (isset($headers['userId'])) {
            $userId= $headers['userId'];
        } else {
            $response['status']['code']=1;
            $response['status']['message']="userId missing";
            return response()->json($response);
        }
          
        $rules = array('visitor_id_img' => 'required|image|mimes:jpg,jpeg,png');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }

      
        $img = $request->file('visitor_id_img');
        $visitor_id_pic=Uploadimage::upload_func($userId, 'visitorphotos', $img);
        $status=1;
               
        DB::collection('visitors')->where('_id', $userId)->update(['visitor_id_img'=> $visitor_id_pic,'status'=> $status,]);

        $response['status']["code"] = 0;
        $response['status']['message']= 'Uploaded successfully.';
        $response['data']['userId']= $userId;
     
        $response['data']['visitor_id_img']= $visitor_id_pic;
        return response()->json($response);
    }
}
