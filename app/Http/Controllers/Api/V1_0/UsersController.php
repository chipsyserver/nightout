<?php
namespace App\Http\Controllers\Api\V1_0;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use Config;
use JWTAuth;
use Hash;
use App\PgStudents;
use App\verifyMobile;
use App\User;
use Image;
require_once app_path().'/Repository/Crypt.php';
require_once app_path().'/Http/Controllers/Api/Sms.php';

class UsersController extends Controller
{
    public function send_otp(Request $request)
    {
        $rules = array('email'=>'required','mobileNumber'=>'required');
        $validator = Validator::make($request->all(), $rules);
        $data = $request->all();
        if ($validator->fails()) {
            $response['status']['code']=1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }

        $usercheck=PgStudents::where('email',$data['email'])->where('mobileNumber', $data['mobileNumber'])->count();

        $response['status']['code']=2;
        $response['status']['message']="No matching data found.";
        if($usercheck==1){
            verifyMobile::where('mobileNumber', $data['mobileNumber'])->delete();

        $verify_code  =rand(1000, 9999);
        $verify_code  =1111;
            // $verify_code    =1111;
            // $data['countryCode']='+91';
        $send_msg= 'Hi '.$verify_code.' is your one time password (OTP),please enter the same to complete your registration process.OTP will get expired in 30 minutes.Thank you';  
        $msg = new \Sms();    
        // $r = $msg->send ($send_msg,$data['mobileNumber']);    
        $verify_code_hash    =base64_encode($verify_code);
        // $user_count          =User::where('mobileNumber', $data['mobileNumber'])->count();
   


         $expirytime=strtotime("+120 minutes");
         $validtime=time() - (5 * 60); // not equal to les than 5 minitus .
              
         $count=verifyMobile::where('mobileNumber', $data['mobileNumber'])
                             ->where('expirytime', '>', $validtime)
                             ->get(array('verifyAuthKey'));
         // die(json_encode( getenv('APP_AUTH_KEY')));
        if (count($count)==0) {
            verifyMobile::where('mobileNumber', $data['mobileNumber'])
            ->delete();

            
                verifyMobile::insertGetId([
                'mobileNumber' => $data['mobileNumber'],
                'verifyAuthKey' => $verify_code_hash,
                'expirytime' => $expirytime
                ]);
               
        // send new otp
        } else {
            // send old otp

        $verify_code=base64_decode($count[0]['verifyAuthKey']);
        }
    

        $response['status']['code']=0;
        $response['status']['message']="OTP sent to mobileNumber successfully.";
        $response['data']['mobileNumber']=$data['mobileNumber'];
        $response['data']['email']=$data['email'];
       
       }
       die(json_encode($response));
    }


    public function verify_user_otp(Request $request)
    {
        $rules     = array(
            'mobileNumber' => 'required',
            'email'        => 'required',
            'otpcode'      => 'required' 
        );

        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            $response['status']['code']    = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }

        $added_at = date('Y-m-d h:i:s');
        $res =  verifyMobile::where('mobileNumber', $data['mobileNumber'])->   get(array('verifyAuthKey','expirytime'));
        $verifyAuthKey = count($res) != 0 ? base64_decode($res[0]['verifyAuthKey']) : null;
        
        if ($data['otpcode']==$verifyAuthKey) {  
            if (time() > $res[0]['expirytime']) {
                $response['status']['code']    = 4;
                $response['status']['message'] = "Token expired try again.";
                return response()->json($response);
            }

            $user_count =PgStudents::where('email',$data['email'])
                        ->where('mobileNumber', $data['mobileNumber'])->get();

            verifyMobile::where('mobileNumber', $data['mobileNumber'])->delete();
            
          

           if (count($user_count)==1) {
                
              
                $credentials = array(
                    'userId' => (string)$user_count[0]['_id'],
                    'mobileNumber' => $user_count[0]['mobileNumber'],
                    'email' => $user_count[0]['mobileNumber']
                );



            $crypt = new \Crypt();
            $crypt->setKey('eff99cfe6876008c6a6e080e4a382be1');
            $crypt->setComplexTypes(true);
            $crypt->setData($credentials);
            $encrypted = $crypt->encrypt();
           
                 
            $response['status']['code']            = 0;
            $response['status']['message']         = "Login successfully.";
            $response['session']['username']       = $user_count[0]['name'];
            $response['session']['mobileNumber']   = $data['mobileNumber'];
            $response['session']['profile_image']  = $user_count[0]['profile_image'];
            $response['session']['email']          = $data['email'];
            $response['session']['expiry_time']    = 1800000;
            $response['session']['Authorizations'] = $encrypted;
            $response['session']['addedTime']      = $added_at;
            $response['session']['updatedTime']    = $added_at;
            return response()->json($response);

            } elseif (count($user_count)) {
                $response['status']['code']    = 3;
                $response['status']['message'] = 'Mobile not registered....';
                return response()->json($response);
            }

            

        } else {
            $response['status']['code']    = 3;
            $response['status']['message'] = "Invalid otp try again.";
            return response()->json($response);
        }
    }

    public function upload_profile(Request $request){
        $rules = array('upload_img' => 'required|image|mimes:jpg,jpeg,png');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }

        $userId=$GLOBALS['userId'];
        $img_cat="profiles";
        $img = $request->file('upload_img');
       

          
        $i_year     =date('Y');
        $i_month    =date('m');
        $image_name =$userId.uniqid();
        $imgtime=time();
       
        if (!is_dir("Backend/". $img_cat)) {
            mkdir("Backend/". $img_cat);
        }

        if (!is_dir("Backend/". $img_cat .  '/' . $i_year)) {
            mkdir("Backend/". $img_cat . '/'. $i_year);
        }

        if (!is_dir("Backend/". $img_cat . '/'.$i_year . "/" . $i_month)) {
            mkdir("Backend/". $img_cat  . '/'. $i_year. "/" . $i_month);
        }
        
        list($width, $height) = getimagesize($img);
        $ratio = $width/$height; // width/height

        if ($ratio > 1) {
            $width = 200;
            $height = 200/$ratio;
        } else {
            $width = 200*$ratio;
            $height = 200;
        }

        $ext = $img->getClientOriginalExtension();
        $path['path'] ='Backend/' . $img_cat . '/' . $i_year . '/' . $i_month . '/' .$imgtime . "_" . $image_name;

        Image::make($img)->resize($width,$height)->save($path['path'].".".$ext);
        $visitors_img=$path['path'].".".$ext;
         
         PgStudents::where('_id',$userId)->update(['profile_image' => $visitors_img]);
         $response['status']['code']    = 0;
         $response['status']['message'] = "profile uploaded successfully.";
         $response['data']['profile_image'] =$visitors_img;
         return response()->json($response); 

    }
}
