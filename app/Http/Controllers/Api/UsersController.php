<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use Config;
use JWTAuth;
use Hash;
require_once app_path().'/Http/Controllers/Api/Sms.php';
require_once app_path().'/Repository/Crypt.php';
class UsersController extends Controller
{
    public function send_otp(Request $request)
    {
        $rules = array('mobileNumber'=>'required');
        $validator = Validator::make($request->all(), $rules);
        $data = $request->all();
      
        if ($validator->fails()) {
            $response['status']['code']=1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }


    
        DB::collection('verify_mobile')->where('mobileNumber', $data['mobileNumber'])->delete();
        $verify_code   =rand(1000, 9999);
       // $verify_code =1111;
     // $verify_code    =1111;
                // $data['countryCode']='+91';
            $send_msg= 'Hi '.$verify_code.' is your one time password (OTP),please enter the same to complete your registration process.OTP will get expired in 30 minutes.Thank you';  
            $msg = new \Sms();    
                   $r = $msg->send ($send_msg,$data['mobileNumber']);    
                
     $verify_code_hash    =base64_encode($verify_code);
        $user_count          =DB::table('users')
                         ->where('mobileNumber', $data['mobileNumber'])->count();
       
    /* if($user_count>0 && $data['type']== 0){
      $response['status']["code"] = 2;
      $response['status']["message"] = 'Mobile number already exist, Sign in continue';
      return response()->json($response);
     }

     if($user_count!=1 && $data['type']== 1){
      $response['status']["code"] = 4;
      $response['status']["message"] = 'Mobile number not exist, Sign up continue.';
      return response()->json($response);
     }
    */
     $expirytime=strtotime("+120 minutes");
        $validtime=time() - (5 * 60); // not equal to les than 5 minitus .
              
         $count=DB::collection('verify_mobile')
                  ->where('mobileNumber', $data['mobileNumber'])
                  ->where('expirytime', '>', $validtime)
                  ->get(array('verifyAuthKey'));
        
        if (count($count)==0) {
            DB::collection('verify_mobile')
          ->where('mobileNumber', $data['mobileNumber'])
          ->delete();

            
            DB::collection('verify_mobile')->insertGetId([
                'mobileNumber' => $data['mobileNumber'],
                'verifyAuthKey' => $verify_code_hash,
                'expirytime' => $expirytime
                ]);
               
        // send new otp
        } else {
            // send old otp

        $verify_code=base64_decode($count[0]['verifyAuthKey']);
        }
    

        $response['status']['code']=0;
        $response['status']['message']="OTP sent successfully.";
        $response['data']['mobileNumber']=$data['mobileNumber'];
    
        die(json_encode($response));
    }


    public function register_func(Request $request)
    {
        $rules     = array(
           
            'mobileNumber' => 'required',
            'otpcode' => 'required'
           
        );

        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            $response['status']['code']    = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }
        $added_at = date('Y-m-d h:i:s');
        
        $res = DB::collection('verify_mobile')->where('mobileNumber', $data['mobileNumber'])->get(array(
            'verifyAuthKey',
            'expirytime'
        ));
        
        
        $verifyAuthKey = count($res) != 0 ? base64_decode($res[0]['verifyAuthKey']) : null;
      
        if ($data['otpcode']==$verifyAuthKey) {
            if (time() > $res[0]['expirytime']) {
                $response['status']['code']    = 4;
                $response['status']['message'] = "Token expired try again.";
                return response()->json($response);
            }

            $user_count          =DB::collection('users')
                                        ->where('mobileNumber', $data['mobileNumber'])->count();
               
                             
            $password=123;
            $message="Login ";
                 
                 
        
             
            if (isset($data['username'])) {
                if ($user_count!=1) {
                    $message="Registration ";
                    $password = Hash::make($password);
                    $res=DB::collection('users')->insert([
                'roleId' => 1,
                'username' => $data['username'],
                'mobileNumber' => $data['mobileNumber'],
                'password'=>$password,
                'added_at' => $added_at,
                'added_at' => $added_at
                ]);
                }
            } elseif ($user_count!=1) {
                $response['status']['code']    = 3;
                $response['status']['message'] = 'Mobile not registered....';
                return response()->json($response);
            }

            DB::collection('verify_mobile')->where('mobileNumber', $data['mobileNumber'])->delete();

            $res=DB::collection('users')->where([
                'mobileNumber' => $data['mobileNumber']
                ]
            )->first();
            
            $credentials = array(
                    'userId' => (string)$res['_id'],
                    'mobileNumber' => $data['mobileNumber'],
                    'password'=>$password,
                    'roleId' => 1
                );

            $crypt = new \Crypt();
            $crypt->setKey('eff99cfe6876008c6a6e080e4a382be1');
            $crypt->setComplexTypes(true);
            $crypt->setData($credentials);
            $encrypted = $crypt->encrypt();
          
        //  die(json_encode(  $encrypted ));

               // die(json_encode(JWTAuth::attempt($credentials)));

                // select required fields
               /* try {
                    // attempt to verify the credentials and create a token for the user
                    if (!$token = JWTAuth::attempt($credentials)) {
                        $response['status']["code"]    = 5;
                        $response['status']["message"] = 'Some thing went wrong.';
                        return response()->json($response);
                    }
                }
                catch (JWTException $e) {
                    // something went wrong whilst attempting to encode the token
                    $response['status']["code"]    = 10;
                    $response['status']["message"] = 'Login fails ,Try again.';
                    return response()->json($response);
                }*/

                $res = DB::collection('users')->where('mobileNumber', $data['mobileNumber'])->get();

                
            $response['status']['code']            = 0;
            $response['status']['message']         = $message." successfully.";
            $response['session']['username']       = $res[0]['username'];
            $response['session']['mobileNumber']   = $data['mobileNumber'];
            $response['session']['expiry_time']    = 1800000;
            $response['session']['Authorizations'] = $encrypted;
             
            $response['session']['addedTime']      = $added_at;
            $response['session']['updatedTime']    = $added_at;
            return response()->json($response);
        } else {
            $response['status']['code']    = 3;
            $response['status']['message'] = "Invalid otp try again.";
            return response()->json($response);
        }
    }
}
