<?php
namespace App\Http\Controllers\Web\Office;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;
use DB;
use Validator;
use Excel;
use Session;
use App\PgStudents;
class ImportExcel extends Controller
{
	
	public function importExcel(Request $request)
	{
		
 $rules = array(
       'import_file' => 'required',
      	 	
   		 );

    
      $validator = Validator::make($request->all(), $rules); 
		$data = $request->all();
	
	    if ($validator->fails()) {
			return back()->with('login-Error', 'Import File is rquired');
	   }




$allowed =  array('xlsx');
$filename = $_FILES['import_file']['name'];
$ext = pathinfo($filename, PATHINFO_EXTENSION);
if(!in_array($ext,$allowed) ) {

   return back()->with('login-Error', 'Extenstion Error | Only xlsx Allowed');
				}

	   			$file = $request->file('import_file');
			//die(json_encode($file->getClientOriginalExtension()));
			
			// die(json_encode($file['import_file']));
        if($file){
            $path = $file->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();



         
		

			if(!empty($data) && $data->count()){
				
		         
		        
$user = new PgStudents;

				foreach ($data as $key =>  $value) {
					

				
					
					$user->rollNo=$value->rollno;
					$user->name=$value->name;
					$user->gender=$value->gender;
					$user->mobileNumber=(string)$value->mobilenumber;
					$user->email=$value->email;
					$user->termCredits=(string)$value->termcredits;
					$user->count=(string)$value->termcredits;
					$user->carryForward=(string)$value->carryforward;
					$user->profile_image="";
					$user->pgp=(string)$value->pgp;
						if($value->placed==TRUE)
						{
						$user->placed="1";
						}
						else {
							$user->placed="1";
							}

	              	
                 $user->save();
                  return back()->with('login-Success', 'successfully Uploaded');
					

				}
				
			}

				
		 Session::flash('login-Error', 'Error');
	
	     }
		
	}
}