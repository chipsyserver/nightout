<?php
namespace App\Http\Controllers\Web\Office;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use PDF;
use View;
use App\User;
use App\PgStudents;


class Admin_Panel extends Controller
{
    

public function office_display(Request $request)
    {
       $nightouts = PgStudents::paginate(5);
          return $nightouts;
      }
    public function office_view(Request $request)
    {
        
 

            return view('Office.pages.excel');
        
    }

    public function users_func(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.users');
        } else {
            $query = $request->query();

            $users = User::query()->where('_id', '!=', Auth::id());

            if ($cond = @$query['smart_query']) {
                $users->where(function ($q) use ($cond) {
                    $q->orWhere('username', 'like', '%'.$cond.'%');
                    $q->orWhere('mobileNumber', 'like', '%'.$cond.'%');
                });
            }

            $users= $users->orderBy('added_at', 'DESC')->paginate(20);

            foreach ($users as $user) {
                $user->registered_on = date('d-m-Y', strtotime($user->added_at));
            }

            return $users;
        }
    }

    public function print_pass($visitor_id='')
    {
        if ($visitor_id) {
            $visitor = Visitor::with('requester')->find($visitor_id);
            $pdf = PDF::loadView('Backend.pdf.visitor_pass', ['visitor'=>$visitor]);
            return $pdf->stream('pass.pdf');
        }
    }
}
