<?php 
namespace App\Http\Controllers\Web\Nightout\Auth;
use App\Http\Controllers\Controller;
// use App\Model\Login;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Session;
use DB;
use Hash;
use Auth;
use Input;
class AuthController extends Controller {



    public function index()
    {
        
        return view('Nightout.login');
    }
    


public function postLogin(Request $request)
    {
        $inputs    = $request->all();

        $rules     = array(
            'username' => 'required|max:255',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        
        $creds = [
            'username'          => $data['username'],
            'password'       => $data['password'],
            "roleId"       => 1002,
            
        ];

        if (Auth::guard('nightout')->attempt($creds, true)) {
            return redirect('/nightout/pgp-office/request');
        } else {
            Session::flash('login-error', 'Invalid username or password');
            Session::flash('loggd-username', $data['username']);

            return redirect('/nightout/pgp-office/login');
        }
    }
    



 	public function settings_view()
    {
        return view('Nightout.accountsetting' );
    }

    public function change_settings(Request $request)
    {
        $rules     = array(
            'username' => 'required',
            'current_password' => 'required'
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }


        if (Hash::check($data['current_password'], Auth::guard('nightout')->user()->password)) {
            $user = User::find(Auth::guard('nightout')->id());

            if ($change_pass) {
                $n_password = Hash::make($data['new_password']);
                $user->password = $n_password;
            }

            $user->username = $data['username'];
            $user->save();
            // Auth::logout();
            Auth::loginUsingId(Auth::id());

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Current password entered is incorrect',
            ));
        }
    }

  public function signout()
    {
        Auth::guard('nightout')->logout();

        return redirect('/nightout/pgp-office/login');
    }
 
 }
 
