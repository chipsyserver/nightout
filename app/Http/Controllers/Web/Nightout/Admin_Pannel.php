<?php 
namespace App\Http\Controllers\Web\Nightout;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Login;
use Hash;
use DB;
USE Input;
use App\RequestNightOut;
use Auth;
class Admin_Pannel extends Controller {
	 	
          

         public function dashboard_func(Request $request)
    {
        $return = [];
        $return['total_users'] = RequestNightOut::whereIn('status',[0,1,-2])->count();
        $return['pending'] = RequestNightOut::where('status', 0)->whereNotIn('status',[-1,2])->count();
        $return['aproved'] = RequestNightOut::where('status', 1)->count();
        $return['rejected'] = RequestNightOut::where('status', -2)->count();

        return view('Nightout.pgp.dashboard', $return);
    }

          	

          // 	public function Nightout_welcome()
          // 	{
          		
          // return view('Nightout.pgp.table' );
          // 	}
            public function Nightout_welcome(Request $request, $render='view',$pgp='0')
            {
                             

           

                if ($render == 'view') {
               

            return view('Nightout.pgp.nightout', ['current_day' => date('d-m-Y')]);
        }
        else if ($pgp !='0' ) {

       
                       $nightouts = RequestNightOut::where('status',(int)$render)->with('requester','user')->whereHas('requester', function ($query) use ($pgp){
                      $query->where('pgp', $pgp);})->paginate(5);


                      foreach ($nightouts as $nightout) {
                              $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                               
                              $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                          }
                   return $nightouts;


               }
              
          
          else if ($render !='0') {
         
               $nightouts = RequestNightOut::where('status',(int)$render)->with('requester','user')->paginate(5);  
                         
             
                  }
                                              
                  
                  else 
                  {
                    
                   
              $nightouts = RequestNightOut::where('status',0)->with('requester')->paginate(5); 
                              
               
                     }


                 foreach ($nightouts as $nightout) {
                        $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                         
                        $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                      
                       // echo $nightout;
                    }
             return $nightouts;
          }

          	public function Nightout_display()
          	{

          		
           $data = DB::collection('request_nightout')->get();
                  return $data;
              }
	
        	public function nightout_func(Request $request, $render='view')
            {
                if ($render == 'view') {
         

                    return view('Nightout.pgp.table', ['current_day' => date('d-m-Y')]);
                }
                else if ($render !='0') {

        
                  // echo $render;
             $nightouts = RequestNightOut::whereNotIn('status',[-1,2,1,-2])->with('requester')->whereHas('requester', function ($query) use ($render){
            $query->where('pgp', $render);
        })->paginate(5);
              
    
                   

                    foreach ($nightouts as $nightout) {
                        $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                         
                        $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                       // echo $nightout;
                    }
                               

                                    // return $nightout;

                    
}
                    

                 else  {
                   

                    
                    $nightouts = RequestNightOut::whereNotIn('status',[-1,2,1,-2])->with('requester')->paginate(5);
                      foreach ($nightouts as $nightout) {
                        $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                         
                        $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                       // echo $nightout;
                    }
                  

                     
                      }     
                  
                    
                     return $nightouts;
                
            }
 public function change_action(Request $request)
         {
          $id=$request->id;
          $action=$request->action;
          $nightout = RequestNightOut::where('_id',$id) ->first();



          if($nightout)
          {
                            
            $nightout->status =(int)$action;
            $nightout->save();

             return response()->json(array(
                              'id'=>$id,
                              'action'=>$id,
                              'message' => " successfully updated"
                              ));
            }
          }

        	public function Nightout_settings()
        	{
        		
        return view('Nightout.accountsetting' );
        	}


         public function change_status(Request $request)
         {
          $id=$request->id;
          $status=$request->status;
          $nightout = RequestNightOut::where('_id',$id) ->first();
          if($nightout)
          {
                  
            $nightout->status =  (int)$status;
            $nightout->save();
             
             return response()->json(array(
                              'id'=>$id,
                              'status'=>$status,
                              'message' => " successfully updated"
                              ));
            }
          }
   
	




	

}
