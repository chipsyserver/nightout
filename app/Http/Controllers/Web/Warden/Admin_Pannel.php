<?php 
namespace App\Http\Controllers\Web\Warden;
use App\Http\Controllers\Controller;
use App\User;
USE App\Pg_Students;
use Illuminate\Http\Request;
use App\RequestNightOut;
use Validator;
use DB;
use Hash;
use Auth;
use Input;
use mail;
class Admin_Pannel extends Controller {
	 	
         public function dashboard_func(Request $request)
    {
        $return = [];
        $return['total_users'] = RequestNightOut::whereIn('status',[-1,2,1])->count();
        $return['pending'] = RequestNightOut::where('status', 1)->whereNotIn('status',[-1,2])->count();
        $return['aproved'] = RequestNightOut::where('status', 2)->count();
        $return['rejected'] = RequestNightOut::where('status', -1)->count();

        return view('Warden.page.dashboard', $return);
    }

    //          public function dashboard_func(Request $request)
    // {
       

    //     return view('Warden.page.dashboard');
    // }
          	
           public function create_func()
            {
              
          return view('Nightout.superadmin.page.user' );
            }
          	public function Nightout_display()
          	{

          		
           $data = DB::collection('request_nightout')->get();
                  return $data;
              }
	

  public function user_func(Request $request)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required',
            'empid' => 'required',
            'group1'=>'required'
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $roleId= 1001;
        $delete=1;

         
           $user = User::where('email', '=', $request->email)->first();
if ($user === null) {
   // user doesn't exist

  $user = new User;
             $n_password = Hash::make($request->new_password);
              $user->username=$request->username;
                $user->password = $n_password;
                $user->sex=$request->group1;
                $user->email=$request->email;
                $user->empid=$request->empid;
                 $user->roleId=$roleId;
                $user->delete=$delete;
                 $user->save();
                  return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
                }

            else
                {
                return response()->json(array(
                'success' => false,
                'message' => 'Email Id already exitsted', ));
              }


  }



  public function update_func(Request $request,$id)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required',
            'empid' => 'required',
            'group1'=>'required'
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        // $roleId= 1001;
        // $delete=1;
        // echo $id;

            $user =User::where('_id', $id)->first();
           
              $user->username=$request->username;
              if($request->new_password!="password")
              {
                 $n_password = Hash::make($request->new_password);
                 $user->password = $n_password;
              }
                
                $user->sex=$request->group1;
                $user->email=$request->email;
                $user->empid=$request->empid;
                 // $user->roleId=$roleId;
                // $user->delete=$delete;
                 $user->save();

                  return response()->json(array(
                'success' => true,
                'message' => 'successfully updated',
            ));
   
         
  
           

  }
      public function change_action(Request $request)
         {
          $id=$request->id;
          $action=$request->action;
         $userid=$request->userid;
          $nightout = RequestNightOut::where('_id',$id) ->first();


  if($nightout)
          {
            $nightout->warden_id=$userid;
                  
            $nightout->status =(int)$action;
            $nightout->wardenComment=$request->comment;
            $nightout->save();
          $student=Pg_Students::where('_id',$userId)->get();
            if($action=="2")
            {
           
             $student->termCredits=$termCredits-1;
             $student->save();

              $user= User::where('_id', $userid)->get();
                  
             
             $to = 'chipsyinfo@gmail.com';
              $subject = 'Tapmi Nightout Approval ';
              $from = 'chipsyinfo@gmail.com';
               
              // To send HTML mail, the Content-type header must be set
              $headers  = 'MIME-Version: 1.0' . "\r\n";
              $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
               
              // Create email headers
              $headers .= 'From: '.$from."\r\n".
                  'Reply-To: '.$from."\r\n" .
                  'X-Mailer: PHP/' . phpversion();
               
              // Compose a simple HTML email message
              $message = '<html><body>';
              // $message .= '<h1 style="color:#f40;"></h1>';
              $message .= '<p style="color:#080;font-size:18px;">Student Name:'+$nightout->name+' <br>Approval by'+ $user[0]['username']+'<br> Date: '+ date("m/d/Y h:i:s a", time()); +'</p>';
              $message .= '</body></html>';
               
              // Sending email
             mail($to, $subject, $message, $headers);
             
            }
            else{

              $student->termCredits=$termCredits+1;
             $student->save();
            }
          }
        
             return response()->json(array(
                              'id'=>$id,
                              'action'=>$id,
                              'message' => " successfully updated"
                              ));
            
            
          }


  public function edituser_details(Request $request, $id)
            {
              $user = User::where('_id', '=', $id)->first();

              return $user;
            }


        public function warden_func(Request $request, $render='view',$pgp='0')
            {
                  $sex='f';
                  $id=Auth::guard('warden')->user()->sex;
                  if($id=='1'){
                    $sex='m';

                  }
                  
                  if ($pgp !='0' ) {

       
                       $nightouts = RequestNightOut::where('status',(int)$render)->with('requester','user')->whereHas('requester', function ($query) use ($pgp,$sex){
                      $query->where('pgp', $pgp);
                      $query->where('gender', $sex);})->paginate(5);


                      foreach ($nightouts as $nightout) {
                              $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                               
                              $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                          }
                   return $nightouts;


               }
           

                if ($render == 'view') {
               

            return view('Warden.page.nightout', ['current_day' => date('d-m-Y')]);
        }

          
          else if ($render !='0') {
         
               $nightouts = RequestNightOut::where('status',(int)$render)->with('requester','user')->whereHas('requester', function ($query) use ($sex){
                $query->where('gender', $sex); })->paginate(5);  
                         
             
                  }
                                              
                  
                  else 
                  {
                    

              $nightouts = RequestNightOut::where('status',1)->with('requester')
              ->whereHas('requester', function ($query) use ($sex){
               $query->where('gender', $sex); })->paginate(5); 
                              
               
                     }


                 foreach ($nightouts as $nightout) {
                        $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                         
                        $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                      
                       // echo $nightout;
                    }
             return $nightouts;
          }
                  
               
  



    public function user_details(Request $request)
    {
        $roleId=1001;
            

            $users = User::where('roleId' ,$roleId  )->paginate(10);

           
           

            foreach ($users as $user) {
                $user->registered_on = date('d-m-Y', strtotime($user->created_at));
            }

            return $users;
        
    }

         public function delete_func(Request $request)
         {
          $id=$request->id;
          $delete=0;

         
          $user = User::where('_id',$id)->first();
              
             $user->delete =$delete ;
             $user->save();
             
             return response()->json(array(
                              'success'=>"true",
                              
                              'message' => " successfully delete"
                              ));
            
          }
   
	




	

}
