<?php
namespace App\Http\Controllers\Web\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use PDF;
use View;
use App\Visitor;
use App\User;


class VMSController extends Controller
{
    public function dashboard_func(Request $request)
    {
        $return = [];
        $return['total_users'] = User::where('_id', '!=', Auth::id())->count();
        $return['total_visitors'] = Visitor::count();
        $return['visited'] = Visitor::where('status', 1)->count();
        $return['non_visited'] = Visitor::where('status', 0)->count();

        return view('Backend.pages.dashboard', $return);
    }

    public function visitors_func(Request $request, $render='view')
    {
        if ($render == 'view') {
 

            return view('Backend.pages.visitors', ['current_day' => date('d-m-Y')]);
        } else {
            $query = $request->query();

            $visitors = Visitor::query()->with('requester');

            if (isset($query['status']) && $query['status'] != '') {
                $visitors->where('status', (int)$query['status']);
            }

            if ($cond = @$query['smart_query']) {
                $visitors->where(function ($q) use ($cond) {
                    $q->orWhere('name', 'like', '%'.$cond.'%');
                    $q->orWhere('mobile_num', 'like', '%'.$cond.'%');
                    $q->orWhere('address', 'like', '%'.$cond.'%');
                });
            }

            if (@$query['date_of_visit']) {
                $date_of_visit = $query['date_of_visit'];
                $visitors->where('date_of_visit', $date_of_visit);
            }

            $visitors = $visitors->orderBy('created_at', 'DESC')->paginate(20);
            foreach ($visitors as $visitor) {
                $visitor->requested_on = date('d-m-Y h:i A', strtotime($visitor->created_at));
            }


            return $visitors;
        }
    }

    public function users_func(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.users');
        } else {
            $query = $request->query();

            $users = User::query()->where('_id', '!=', Auth::id());

            if ($cond = @$query['smart_query']) {
                $users->where(function ($q) use ($cond) {
                    $q->orWhere('username', 'like', '%'.$cond.'%');
                    $q->orWhere('mobileNumber', 'like', '%'.$cond.'%');
                });
            }

            $users= $users->orderBy('added_at', 'DESC')->paginate(20);

            foreach ($users as $user) {
                $user->registered_on = date('d-m-Y', strtotime($user->added_at));
            }

            return $users;
        }
    }

    public function print_pass($visitor_id='')
    {
        if ($visitor_id) {
            $visitor = Visitor::with('requester')->find($visitor_id);
            $pdf = PDF::loadView('Backend.pdf.visitor_pass', ['visitor'=>$visitor]);
            return $pdf->stream('pass.pdf');
        }
    }
}
