<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/hash', 'UserController@run');
// die(json_encode("value"));



Route::group(['middleware' => ['guest:superadmin']], function () {
    Route::get('/login','AuthController@index');
    Route::get('/', function () {
        return redirect('superadmin/login');
    });
    Route::post('/auth-login', 'AuthController@auth_login');
});


 
	   


Route::get('/signout', 'AuthController@signout');

Route::group(['middleware' =>'superadmin'] , function () {
	 Route::get('/create', 'Admin_Pannel@create_func');
	 Route::get('/dashbord', 'Admin_Pannel@dashboard_func');
	 Route::post('/user', 'Admin_Pannel@user_func');
	 Route::get('/userdetails', 'Admin_Pannel@user_details');
	  Route::post('/delete', 'Admin_Pannel@delete_func');
	   Route::get('/edituser/{id}', 'Admin_Pannel@edituser_details');
	    Route::post('/updateuser/{id}', 'Admin_Pannel@update_func');
	     Route::get('/settings', 'AuthController@settings_view');
	      Route::post('/sub-settings', 'AuthController@change_settings');
	    
	 
	 Route::get('display', 'Admin_Pannel@nightout_func');
	Route::get('display/{render}', 'Admin_Pannel@nightout_func');
	Route::get('nightout', 'Admin_Pannel@display_func');
	Route::get('nightout/{render}', 'Admin_Pannel@display_func');
	//  Route::post('/status', 'Admin_Pannel@change_status');

 // Route::get('request', 'Admin_Pannel@nightout_welcome');



 




});




