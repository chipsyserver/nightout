<?php

Route::get('/signout', 'AuthController@signout');


Route::group(['middleware' => ['guest:warden']], function () {
    Route::get('/login','AuthController@index');
    Route::get('/', function () {
        return redirect('/warden/login');
    });
   Route::post('auth-login', 'AuthController@auth_login');
});

// Route::get('/', 'AuthController@index');


Route::post('auth-login', 'AuthController@auth_login');
    
    Route::group(['middleware' => ['warden']], function () {
     Route::get('/settings', 'AuthController@settings_view');
    Route::post('/sub-settings', 'AuthController@change_settings');
    
   Route::get('/nightout', 'Admin_Pannel@warden_func');
	 Route::get('/dashbord', 'Admin_Pannel@dashboard_func');
	  Route::get('/nightout', 'Admin_Pannel@warden_func');
	    Route::get('/nightout/{render}', 'Admin_Pannel@warden_func');
        Route::get('/nightout/{render}/{pgp}', 'Admin_Pannel@warden_func');
	     Route::post('/action', 'Admin_Pannel@change_action');
        // Route::get('/home', 'VMSController@dashboard_func');
        // Route::get('/visitors', 'VMSController@visitors_func');
        // Route::get('/visitors/{data}', 'VMSController@visitors_func');
        // Route::get('/users', 'VMSController@users_func');
        // Route::get('/users/{data}', 'VMSController@users_func');

        // Route::get('/print-pass/{visitor_id}', 'VMSController@print_pass');
   
});
