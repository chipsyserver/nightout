<?php

Route::get('/signout', 'AuthController@signout');

Route::group(['middleware' => ['guest:office']], function () {
    Route::get('/login', [ 'as' => 'login', 'uses' => 'AuthController@index']);
    Route::get('/', function () {
        return redirect('/login');
    });
    Route::post('/auth-login', 'AuthController@auth_login');
});
    

Route::group(['middleware' => ['office']], function () {
    Route::get('/settings', 'AuthController@settings_view');
    Route::post('/sub-settings', 'AuthController@change_settings');
    Route::get('/excel', 'Admin_Panel@office_view');
     Route::post('/importExcel', 'ImportExcel@importExcel');
        Route::get('/display', 'Admin_Panel@office_display');

    });