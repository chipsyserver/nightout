<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/version/{id}', 'Api\AppControl@vesion_control');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['visit_auth']], function () {
    Route::post('/bookentry', 'Api\BookVisitTime@bookvisit_func');
});


Route::get('/bookedtoday', 'Api\BookVisitTime@bookedtoday_func');
Route::post('/visitor-upload', 'Api\BookVisitTime@upload_visitor_pic_func');
Route::post('/visitor-id-upload', 'Api\BookVisitTime@upload_visitor_id_pic_func');
Route::post('/send-otp', 'Api\UsersController@send_otp');
Route::post('/login-or-register', 'Api\UsersController@register_func');


Route::group(['prefix' => 'v1.0','namespace' => 'Api\V1_0'],function (){
   Route::post('/send-otp', 'UsersController@send_otp');
   Route::post('/verify-user-otp', 'UsersController@verify_user_otp');

   Route::group(['middleware' => ['app_auth']], function () {
     Route::post('/upload-profile','UsersController@upload_profile');
     Route::post('/request-nightout','RequestController@request_nightout'); 
     Route::get('/getrequest-nightout','RequestController@get_request_nightout'); 
     Route::get('/getrequest-nightout/{id}','RequestController@get_request_id'); 
     Route::post('/post-lateentry','RequestController@post_laste_entry');
   });

});
